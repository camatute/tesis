<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informacione extends Model
{
    //
    public function user(){
        return $this->belongsTo('App\User');
    }

    protected $fillable=[
        'imagen', 'id_t', 'nombre', 'apellido', 'telefono', 'edad', 'direccion', 'campo',
    ];
}
