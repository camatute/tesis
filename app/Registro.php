<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    //
    public function palabras(){
        // return $this->belongsToMany('User','id_p', 'id');
        return $this->hasMany('App\Palabra','id_r', 'id');
    }

    public function difonos(){
        // return $this->belongsToMany('User','id_p', 'id');
        return $this->hasMany('App\Difono','id_r', 'id');
    }

    public function diptongos(){
        // return $this->belongsToMany('User','id_p', 'id');
        return $this->hasMany('App\Diptongo','id_r', 'id');
    }

    public function silabasinversas(){
        // return $this->belongsToMany('User','id_p', 'id');
        return $this->hasMany('App\Silabasinversa','id_r', 'id');
    }
    
    public function paciente(){
        return $this->belongsTo('App\Paciente', 'id_p');
    }


    protected $fillable = [
        'id_p', 'fecha', 'observacion', 'recomendacion', 'conclusion'
    ];

    public function scopeName($query, $name){
        if(trim($name)!=""){
            $query->where('id_p', "LIKE", "%$name%");
            }
        }
}