<?php

namespace App\Http\Controllers;

use App\Terapia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Storage;
use DB;

class TratamientosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        // $tratamientos = DB::table('terapias')->get();
        // $tratamientos= Terapia::name($request->get('search'))->orderBy('id')
        $tratamientos = Terapia::name($request->get('search'))->with('users')->where('id_u', Auth::id())->orderBy('id')
        ->paginate(6);
        return view('Tratamiento/tratamiento',compact('tratamientos'));
    }

     public function tratamiento_lista(Request $request)
    {
        //
        // $tratamientos = Terapia::paginate(6);
        $tratamientos = Terapia::name($request->get('search'))->with('users')->where('id_u', Auth::id())->paginate(5);
        $title = "Listado de Registros";

        return view('Tratamiento/tratamiento_lista', compact('title', 'tratamientos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Tratamiento/tratamiento_crear');
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules= [
            'imagen' => 'required',
            'palabra' => 'required',
        ];

        $this->validate($request, $rules);
        
        if($name = $request->file('audio')){
            $file=$request->file('audio');
           $name=time().$file->getClientOriginalName();
           $file->move(public_path().'/sound/', $name);
           
         }        
         
        if($img = $request->file('imagen')){
            $file=$request->file('imagen');
           $img=time().$file->getClientOriginalName();
           $file->move(public_path().'/imagen/', $img);
           
         }        
           
         $tratamiento = new Terapia();
        
          
           $tratamiento->palabra = $request->palabra;
           $tratamiento->audio = $name;
           $tratamiento->imagen = $img;
           $tratamiento->id_u = Auth::user()->id;
           
        //    $img = $request->file('imagen');

        //    $file_route = time().'_'.$img->getClientOriginalName();
        //    Storage::disk('imagen')->put($file_route,file_get_contents($img->getRealPath()));
           
    
           if($tratamiento->save()){
           return redirect()->route('tratamiento.index')->with('exito',"REGISTRO GUARDADO EXITOSAMENTE");
           }else{
               return redirect()->route('tratamiento.index')->with('error',"ERROR AL GUARDAR UN PACIENTE");
         }
     }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tratamiento  $tratamiento
     * @return \Illuminate\Http\Response
     */
    public function show(Terapia $tratamiento)
    {
        //
        return view('Tratamiento/tratamiento_show', compact('tratamientos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tratamiento  $tratamiento
     * @return \Illuminate\Http\Response
     */
    public function edit(Terapia $tratamiento)
    {
        //
        return view('Tratamiento/tratamiento_editar', compact('tratamiento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tratamiento  $tratamiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Terapia $tratamiento)
    {
        $rules= [
            'imagen' => 'required',
            'palabra' => 'required',
        ];

        $this->validate($request, $rules);


        $tratamiento->fill($request->except('imagen','audio'));
        if($name = $request->file('audio')){
            $file=$request->file('audio');
           $name=time().$file->getClientOriginalName();
           $tratamiento->audio = $name;
           $file->move(public_path().'/sound/', $name);
           
         }        
         
        if($img = $request->file('imagen')){
            $file=$request->file('imagen');
           $img=time().$file->getClientOriginalName();
           $tratamiento->imagen = $img;
           $file->move(public_path().'/imagen/', $img);
           
         }        
        
    
           if($tratamiento->update()){
        return redirect()->route('tratamiento.index')->with('exito',"TRATAMIENTO GUARDADO EXITOSAMENTE");
           }else{
               return redirect()->route('tratamiento.index')->with('error',"ERROR AL GUARDAR TRATAMIENTO");
         }
           
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tratamiento  $tratamiento
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tratamiento = Terapia::find($id)->delete();
        return redirect()->route('tratamiento.index')->with('exito',"Eliminado Exitoso");

    }
    }

