<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;




use DB;

class InformacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Users = DB::table('users')->get();
        return view('Users/user_list', compact('Users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Users/user_crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $rules= [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8']
                ];

      
        $this->validate($request, $rules);

        $usuario = new User();
        $usuario->name = $request->name;
        $usuario->email = $request->email;
        $usuario->password = bcrypt($request->password);

        if($usuario->save()){
            return redirect()->route('usuarios.index')->with('exito',"REGISTRO GUARDADO EXITOSAMENTE");
            }else{
                return redirect()->route('usuarios.index')->with('error',"ERROR AL GUARDAR UN PACIENTE");
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Informacione  $informacione
     * @return \Illuminate\Http\Response
     */
    public function show(Users $usuarios)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Informacione  $informacione
     * @return \Illuminate\Http\Response
     */
    public function edit(User $usuario)
    {
        return view('Users/user_editar', compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Informacione  $informacione
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $usuario)
    {
        $rules= [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
        ];

     
       $this->validate($request, $rules);
       
        $usuario->name = $request->name;
        $usuario->email = $request->email;
        // $usuario->password = bcrypt($request->password);
        // $usuario->password = bcrypt($request->password_new);        
        if($usuario->update()){
            return redirect()->route('usuarios.index')->with('exito',"REGISTRO GUARDADO EXITOSAMENTE");
            }else{
                return redirect()->route('usuarios.index')->with('error',"ERROR AL GUARDAR UN PACIENTE");
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Informacione  $informacione
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuarios = User::find($id)->delete();
        return redirect()->route('usuarios.index')->with('exito',"Eliminado Exitoso");
    }
}