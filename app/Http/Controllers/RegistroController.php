<?php

namespace App\Http\Controllers;
use App\Registro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Paciente;
use App\Palabra;
use App\Diptongo;
use App\Difono;
use App\Silabasinversa;
use PDF;


class RegistroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Registro
     */
    public function index(Request $request)
    {
        //llamado de informacion con funcion
        $evaluacion = Registro::name($request->get('search'))
        ->with('paciente')->whereHas('paciente', function ($query) {
            $query->where('id_u', '=', Auth::id());
        })
        // ->latest()
        ->orderBy('id')->paginate(6);
        
        // dd($evaluaciones);

        // $evaluacion = Registro::name($request->get('search'))->paginate(6);
        return view('Evaluacion/evaluacion_lista', compact( 'evaluacion'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = Paciente::all()->where('id_u', Auth::id());
        return view('Evaluacion/evaluacion_crear', compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $rules= [
              'fecha' => 'required',
              'observacion' => 'required',
              'recomendacion' => 'required',
              'conclusion' => 'required',
            'id_p'=> 'required',
  

        //    'estimulo' => 'required|string|max:255',
        //    'fonema' => 'required|string|max:255',
        //    'inicial' => 'required|string|max:255',
        //    'media' => 'required|string|max:255',
        //    'final' => 'required|string|max:255',
        //    'transicion' => 'required|string|max:255',
        //    'respuesta' => 'required|string|max:255',

        //    'difonos' => 'required|string|max:255',
        //    'estimulodf' => 'required|string|max:255',
        //    'respuestadf' => 'required|string|max:255',
        //    'transiciondf' => 'required|string|max:255',

        //    'diptongo' => 'required|string|max:255',
        //    'estimulod' => 'required|string|max:255',
        //    'respuestad' => 'required|string|max:255',
        //    'transiciond' => 'required|string|max:255',

        //    'silaba' => 'required|string|max:255',
        //    'estimulods' => 'required|string|max:255',
        //    'respuestads' => 'required|string|max:255',
        //    'transicionds' => 'required|string|max:255',


 
       ];

     
        $this->validate($request, $rules);

       
        


        $mensaje = new Registro();
        $mensaje->id_p = $request->id_p;
        $mensaje->fecha = $request->fecha; // base de datos - formulario
        $mensaje->observacion= $request->observacion;
        $mensaje->recomendacion= $request->recomendacion;
        $mensaje->conclusion = $request->conclusion;

        $palabras = $request->all()['palabras'];
        $diptongos = $request->all()['diptongos'];
        $difonos = $request->all()['difonos'];
        $silabas = $request->all()['silabas'];

        if($mensaje->save()){
            foreach($palabras as $palabra){
                $palabra['id_r'] = $mensaje->id;
                Palabra::create($palabra);   
            }
            foreach($diptongos as $diptongo){
                $diptongo['id_r'] = $mensaje->id;
                Diptongo::create($diptongo);   
            }
            foreach($difonos as $difono){
                $difono['id_r'] = $mensaje->id;
                Difono::create($difono);   
            }
            foreach($silabas as $silaba){
                $silaba['id_r'] = $mensaje->id;
                Silabasinversa::create($silaba);   
            }
        return redirect()->route('evaluacion.index')->with('exito',"REGISTRO GUARDADO EXITOSAMENTE");
        }else{
            return redirect()->route('evaluacion.index')->with('error',"ERROR AL GUARDAR UN PACIENTE");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Registro $evaluacion)
    {
        $evaluacion = Registro::with('palabras', 'difonos', 'diptongos','silabasinversas')->where('id', $evaluacion->id)->get();

        if (count($evaluacion) >= 1) {
            $evaluacion = $evaluacion[0];
        }
        
        $groups = Paciente::all()->where('id_u', Auth::id());
        $palabras = Palabra::all();
        $difonos = Difono::all();
        $diptongos = Diptongo::all();
        $silabasinversas = Silabasinversa::all();
        $pacientes = Paciente::find($evaluacion['id_p']);

        
        return view('Evaluacion/evaluacion_editar', compact('evaluacion','groups','palabras','difonos','diptongos','silabasinversas','pacientes'));
        
        // return view('Pacientes/paciente_editar', compact('paciente'))->with('pacientes');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Registro $evaluacion)
    {
        // dd($paciente);
        // dd($request);

        $rules= [
            'fecha' => 'required',
            'observacion' => 'required',
            'recomendacion' => 'required',
            'conclusion' => 'required',
            'id_p'=> 'required',
  
          


      //    'estimulo' => 'required|string|max:255',
      //    'fonema' => 'required|string|max:255',
      //    'inicial' => 'required|string|max:255',
      //    'media' => 'required|string|max:255',
      //    'final' => 'required|string|max:255',
      //    'transicion' => 'required|string|max:255',
      //    'respuesta' => 'required|string|max:255',

      //    'difonos' => 'required|string|max:255',
      //    'estimulodf' => 'required|string|max:255',
      //    'respuestadf' => 'required|string|max:255',
      //    'transiciondf' => 'required|string|max:255',

      //    'diptongo' => 'required|string|max:255',
      //    'estimulod' => 'required|string|max:255',
      //    'respuestad' => 'required|string|max:255',
      //    'transiciond' => 'required|string|max:255',

      //    'silaba' => 'required|string|max:255',
      //    'estimulods' => 'required|string|max:255',
      //    'respuestads' => 'required|string|max:255',
      //    'transicionds' => 'required|string|max:255',



     ];

   
      $this->validate($request, $rules);

        $evaluacion->id_p = $request->id_p;
        $evaluacion->fecha = $request->fecha; // base de datos - formulario
        $evaluacion->observacion= $request->observacion;
        $evaluacion->recomendacion= $request->recomendacion;
        $evaluacion->conclusion = $request->conclusion;

          if($evaluacion->update()){
          $evaluacion->palabras()->delete();//Miembro::where('id_p', $paciente->id)->delete();
          $palabras = $request->input('palabras', []);
        //   dd($palabras);
 
         foreach($palabras as $palabra){
            $palabra['id_r'] = $evaluacion->id;
            Palabra::create($palabra);   
        }

         $evaluacion->diptongos()->delete();
         $diptongos = $request->input('diptongos', []);
         foreach($diptongos as $diptongo){
             $diptongo['id_r'] = $evaluacion->id;
             Diptongo::create($diptongo);   
         }

             $evaluacion->difonos()->delete();
             $difonos = $request->input('difonos', []);
             foreach($difonos as $difono){
             $difono['id_r'] = $evaluacion->id;
             Difono::create($difono);  
          }

         $evaluacion->silabasinversas()->delete();
         $silabasinversas = $request->input('silabasinversas',[]);
         foreach($silabasinversas as $silabasinversa){
             $silabasinversa['id_r'] = $evaluacion->id;
             Silabasinversa::create($silabasinversa);   
         
            }

        return redirect()->route('evaluacion.index')->with('exito',"EVALUACION GUARDADO EXITOSAMENTE");
    
}else{
        return redirect()->route('evaluacion.index')->with('exito',"EVALUACION NO SE REGISTRO");
    }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $evaluacions = Registro::find($id)->delete();
        return redirect()->route('evaluacion.index')->with('exito',"Eliminado Exitoso");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function pdfexport(Request $request, Registro $evaluacion){

        $evaluacion = Registro::with('palabras', 'difonos', 'diptongos','silabasinversas')->where('id', $evaluacion->id)->get();

       
        if (count($evaluacion) >= 1) {
            $evaluacion = $evaluacion[0];
        }
        
        $pacientes = Paciente::find($evaluacion['id_p']);
        $palabras = Palabra::all();
        $difonos = Difono::all();
        $diptongos = Diptongo::all();
        $silabasinversas = Silabasinversa::all();

        $pdf = \App::make('dompdf.wrapper');
        /* Careful: use "enable_php" option only with local html & script tags you control.
        used with remote html or scripts is a major security problem (remote php injection) */
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf = PDF::loadView('Evaluacion\pdf', compact('evaluacion','pacientes','palabras', 'difonos', 'diptongos','silabasinversas'));
        // $pdf = PDF::loadView('Pacientes\pdf', ['paciente' => $paciente]);
       
       
        $fileName = $evaluacion->id_p;
        $fileName = $evaluacion->fecha;
        $fileName = $evaluacion->recomendacion;
        $fileName = $evaluacion->conclusion;

        return $pdf->stream();
    }
}