<?php

namespace App\Http\Controllers;
use App\Terapia;
use Illuminate\Http\Request;
use Storage;
use DB;
class TerapiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $terapias = Terapia::all();
        return view('Tratamiento/tratamiento',compact('terapias'));
    }

    public function tratamiento_lista(Request $request)
    {
        //
        $terapias = DB::table('terapias')->get();
        $terapias = Terapia::name($request->get('search'))->paginate(6);
        $title = "Listado de Registros";

        return view('Tratamiento/tratamiento_lista', compact('title', 'terapias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       
        return view('Tratamiento/tratamiento_crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        if( $name = $request->file('audio')){
            $file=$request->file('audio');
           $name=time().$file->getClientOriginalName();
           $file->move(public_path().'/sound/', $name);
           
         }    
           
         $terapia = new Terapia();
        
          
           $terapia->palabra = $request->palabra;
           $terapia->audio = $name;


           $img = $request->file('imagen');

           $file_route = time().'_'.$img->getClientOriginalName();
           Storage::disk('imagen')->put($file_route,file_get_contents($img->getRealPath()));
           $terapia->imagen = $file_route;
           
    
           if($terapia->save()){
           return redirect()->route('tratamiento.index')->with('exito',"REGISTRO GUARDADO EXITOSAMENTE");
           }else{
               return redirect()->route('tratamiento.index')->with('error',"ERROR AL GUARDAR UN PACIENTE");
         }
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Terapia $terapia)
    {
        //
        return view('Tratamiento/tratamiento_show', compact('terapia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Terapia $terapia)
    {
        //
        return view('Tratamiento/tratamiento_editar', compact('terapia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Terapia $terapia)
    {
        //
        if( $name = $request->file('audio')){
            $file=$request->file('audio');
           $name=time().$file->getClientOriginalName();
           $file->move(public_path().'/sound/', $name);
           
         }    
            
          
           $terapia->palabra = $request->palabra;
           $terapia->audio = $name;


           $img = $request->file('imagen');

           $file_route = time().'_'.$img->getClientOriginalName();
           Storage::disk('imagen')->put($file_route,file_get_contents($img->getRealPath()));
           $terapia->imagen = $file_route;
           
    
           if($terapia->update()){
           return redirect()->route('tratamiento.index')->with('exito',"REGISTRO GUARDADO EXITOSAMENTE");
           }else{
               return redirect()->route('tratamiento.index')->with('error',"ERROR AL GUARDAR UN PACIENTE");
         }
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $terapia = Terapia::find($id)->delete();
        return redirect()->route('tratamiento.index')->with('exito',"Eliminado Exitoso");

    }
}
