<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Paciente;

class PDFController extends Controller
{
    //
    public function PDFgenerator(){
        $pdf = PDF::loadview('paciente_show', $users);
        return $pdf->download('paciente.pdf');
    }
}
