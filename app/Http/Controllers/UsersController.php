<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Crypt;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $Users = User::name($request->get('search'))->paginate(5);
       return view('Informacion/informacion',compact('Users'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $perfil = User::find(Auth::id());
        if(empty($perfil)){ 
           Flash::error('mensaje error');
           return redirect()->back();
        }
        return view('Informacion/informacion_editar', compact('perfil'))->with('usuario');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $perfil)
    {
        //nombre./ => required
        $rules= [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
        ];

     
       $this->validate($request, $rules);
        $perfil = User::find(Auth::User()->id);
        if(empty($perfil)){
           //Flash::error('mensaje error');
           return redirect()->back();
        }
        //$perfil->fill($request->all());
        $perfil->name = $request->name;
        $perfil->email = $request->email;
        // $perfil->password = $request->password;
        
        $perfil->update();
        //Flash::success('Perfil actualizado con éxito.');
        return redirect(route('home'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
