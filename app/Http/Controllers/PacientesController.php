<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Paciente;
use App\User;
use App\Miembro;
use PDF;

class PacientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pacientes = Paciente::name($request->get('search'))->with('users')->where('id_u', Auth::id())->paginate(5);
        // $tratamientos= Terapia::name($request->get('search'))->orderBy('id','DESC')
        // ->paginate(2);
        $title = "Listado de Registros";
        return view('Pacientes/paciente_lista', compact('title', 'pacientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Pacientes/paciente_crear');
    }

    protected function validator(array $data)
    { 
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $rules= [
             'nombre' => 'required',
             'f_nacimiento' => 'required',
             'edad' => 'required|integer',
             'referencia' => 'required',
             'motivo' => 'required',
             'telefono' => 'numeric',
             'fonacion' => 'max:2',
             'lenguaje' => 'max:2',
             'habla' => 'max:2',
             'audicion' => 'max:2',
         ];

      
        $this->validate($request, $rules);

        $mensaje = new Paciente();
        $mensaje->nombre = $request->nombre; // base de datos - formulario
        $mensaje->f_nacimiento = $request->f_nacimiento;
        $mensaje->edad = $request->edad;
        $mensaje->direccion = $request->direccion;
        $mensaje->telefono = $request->telefono;
        $mensaje->referencia = $request->referencia;
        $mensaje->motivo = $request->motivo;
        $mensaje->fonacion = $request->fonacion;
        $mensaje->lenguaje = $request->lenguaje;
        $mensaje->habla = $request->habla;
        $mensaje->audicion = $request->audicion;
        $mensaje->otro = $request->otro;
        $mensaje->n_madre = $request->n_madre;
        $mensaje->e_madre = $request->e_madre;
        $mensaje->o_madre = $request->o_madre;
        $mensaje->n_padre = $request->n_padre;
        $mensaje->e_padre = $request->e_padre;
        $mensaje->o_padre = $request->o_padre;
        $mensaje->id_u = Auth::user()->id;

        $mensaje->observacion = $request->observacion;
        $mensaje->recomendacion = $request->recomendacion;
        $mensaje->conclusion = $request->conclusion;
        
        $miembros = $request->all()['miembros'];

        if($mensaje->save()){
            foreach($miembros as $miembro){
                    $miembro['id_p'] = $mensaje->id;
                Miembro::create($miembro);   
            }
        return redirect()->route('paciente.index')->with('exito',"REGISTRO GUARDADO EXITOSAMENTE");
        }else{
            return redirect()->route('paciente.index')->with('error',"ERROR AL GUARDAR UN PACIENTE");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Paciente $paciente)
    {
            return view('Pacientes/paciente_show', compact('paciente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Paciente $paciente)
    {
        $paciente = Paciente::with('miembros')->where('id', $paciente->id)->get();

        if (count($paciente) >= 1) {
            $paciente = $paciente[0];
        }
        
        $miembros = Miembro::all();     
        return view('Pacientes/paciente_editar', compact('paciente', 'miembros'));
        
        // return view('Pacientes/paciente_editar', compact('paciente'))->with('pacientes');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paciente $paciente)
    {
        // dd($paciente);
        // dd($request);

        $rules= [
            'nombre' => 'required',
            'f_nacimiento' => 'required',
            'edad' => 'required|integer',
            'motivo' => 'required',
            'telefono' => 'numeric',
            'fonacion' => 'max:2',
            'lenguaje' => 'max:2',
            'habla' => 'max:2',
            'audicion' => 'max:2',
        ];

     
       $this->validate($request, $rules);
        
        $paciente->nombre = $request->nombre; // base de datos - formulario
        $paciente->f_nacimiento = $request->f_nacimiento;
        $paciente->edad = $request->edad;
        $paciente->direccion = $request->direccion;
        $paciente->telefono = $request->telefono;
        $paciente->referencia = $request->referencia;
        $paciente->motivo = $request->motivo;
        $paciente->fonacion = $request->fonacion;
        $paciente->lenguaje = $request->lenguaje;
        $paciente->habla = $request->habla;
        $paciente->audicion = $request->audicion;
        $paciente->otro = $request->otro;
        $paciente->n_madre = $request->n_madre;
        $paciente->e_madre = $request->e_madre;
        $paciente->o_madre = $request->o_madre;
        $paciente->n_padre = $request->n_padre;
        $paciente->e_padre = $request->e_padre;
        $paciente->o_padre = $request->o_padre;
        

        $paciente->observacion = $request->observacion;
        $paciente->recomendacion = $request->recomendacion;
        $paciente->conclusion = $request->conclusion;
    
        // if($paciente->update()){
        // return redirect()->route('paciente.index')->with('exito',"REGISTRO GUARDADO EXITOSAMENTE");
        // }else{
        //     return redirect()->route('paciente.index')->with('error',"ERROR AL GUARDAR UN PACIENTE");
        // }
         $paciente->update();
         $paciente->miembros()->delete();//Miembro::where('id_p', $paciente->id)->delete();
         $miembros = $request->input('miembros', []);
          
         foreach($miembros as $miembro){
            $miembro['id_p'] = $paciente->id;
            Miembro::create($miembro);   
        }
        return redirect()->route('paciente.index')->with('exito',"REGISTRO GUARDADO EXITOSAMENTE");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        $paciente = Paciente::find($id)->delete();
        return redirect()->route('paciente.index')->with('exito',"Eliminado Exitoso");

        // $pacientes = DB::table('pacientes')->get();
        // $title = "Listado de Registros";

        // return view('Pacientes/paciente_lista', compact('title', 'pacientes'));

        // if($id->delete()){
        //     return redirect()->route('paciente.index')->with('exito',"REGISTRO GUARDADO EXITOSAMENTE");
        //     }else{
        //         return redirect()->route('paciente.index')->with('error',"ERROR AL GUARDAR UN PACIENTE");
        //     }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function pdfexport(Request $request, Paciente $paciente){

        
        // $paciente = Paciente::find($id);
        $paciente = Paciente::with('miembros')->where('id', $paciente->id)->get();

        if (count($paciente) >= 1) {
            $paciente = $paciente[0];
        }
        $miembros = Miembro::all();
        
        $pdf = \App::make('dompdf.wrapper');
        /* Careful: use "enable_php" option only with local html & script tags you control.
        used with remote html or scripts is a major security problem (remote php injection) */
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf = PDF::loadView('Pacientes\pdf', compact('paciente', 'miembros'));
        // $pdf = PDF::loadView('Pacientes\pdf', ['paciente' => $paciente]);
       
        $fileName = $paciente->name;
        $fileName = $paciente->f_nacimiento;
        $fileName = $paciente->edad;
        $fileName = $paciente->direccion;
        $fileName = $paciente->telefono;
        $fileName = $paciente->referencia;
        $fileName = $paciente->motivo;
        $fileName = $paciente->fonacion;
        $fileName = $paciente->lenguaje;
        $fileName = $paciente->habla;
        $fileName = $paciente->audicion;
        $fileName = $paciente->otro;
        $fileName = $paciente->n_madre;
        $fileName = $paciente->e_madre;
        $fileName = $paciente->o_madre;
        $fileName = $paciente->n_padre;
        $fileName = $paciente->e_padre;
        $fileName = $paciente->o_padre;


        $fileName = $paciente->observacion;
        $fileName = $paciente->recomendacion;
        $fileName = $paciente->conclusion;
        

        return $pdf->stream();
    }
}