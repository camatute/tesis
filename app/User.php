<?php

namespace App;


use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\UserResetPassword;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function paciente(){
        // return $this->belongsToMany('User','id_p', 'fillable');
        return $this->hasMany('App\'Paciente');
    }
    
    public function terapia(){
        // return $this->belongsToMany('User','id_p', 'fillable');
        return $this->hasMany('App\'Terapia');
    }

    public function informacione(){
        return $this->hasOne('App\Informacione');
    }

    public function scopeName($query, $name){
        if(trim($name)!=""){
            $query->where('email', "LIKE", "%$name%");
            }
        }
}
