<?php

namespace App;
use DB;


use Illuminate\Database\Eloquent\Model;

class Palabra extends Model
{
    // public function paciente(){
    //     return $this->belongsTo(Paciente :: class);
    // }

    public function registros(){
        return $this->belongsTo('App\Registro', 'id_r');
    }

    protected $fillable =[
        'id_r', 'estimulo', 'fonema', 'inicial', 'media', 'final', 'transicion', 'respuesta',];
}
