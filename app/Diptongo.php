<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diptongo extends Model
{
    //
    public function registro(){
        return $this->belongsTo('App\Registro', 'id_r');
    }

    protected $fillable =[
        'id', 'id_r', 'diptongo', 'estimulod', 'respuestad', 'transiciond',];
}

