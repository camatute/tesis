<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terapia extends Model
{
    public function users(){
        return $this->belongsTo('App\User', 'id_u', 'id');
    }
    protected $fillable =[
        'imagen','palabra','audio',];

        public function scopeName($query, $date){
            if(trim($date)!=""){
                $query->where('palabra', "LIKE", "%$date%");

            }
        }
    }