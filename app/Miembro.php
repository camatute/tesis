<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Miembro extends Model
{
    //
    public function paciente(){
        return $this->belongsTo('App\Paciente', 'id_p');
    }

    // public function paciente(){
    //     return $this->belongsTo('App\Paciente', 'id_p', 'id');
    // }

    protected $fillable =[
        'id_p', 'm_casa', 'm_edad', 'm_sexo', 'm_relniño',];
}
