<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Difono extends Model
{
    //
    public function registro(){
        return $this->belongsTo('App\Registro', 'id_r');
    }
    
    protected $fillable =[
        'id_r', 'difonos', 'estimulodf', 'respuestadf', 'transiciondf',];
}

