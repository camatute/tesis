<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Silabasinversa extends Model
{
    //
    public function registro(){
        return $this->belongsTo('App\Registro', 'id_r');
    }

    protected $fillable =[
        'id_r', 'silabas', 'estimulods', 'respuestads', 'transicionds',];

}
