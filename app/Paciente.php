<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    // Padre - hijo
    public function registros()
    {
        return $this->hasMany('App\Paciente', 'id_p', 'id');
    }

    // hijo - padre
    public function users(){
        return $this->belongsTo('App\User', 'id_u', 'id');
    }

    public function miembros(){
        // return $this->belongsToMany('User','id_p', 'id');
        return $this->hasMany('App\Miembro','id_p', 'id');
    }
    
    protected $fillable =[
        'nombre','id_u', 'f_nacimiento','edad','direccion','telefono','referencia','motivo','fonacion','lenguaje','habla','audicion','otro','n_madre','e_madre','o_madre','n_padre','e_padre','o_padre','observacion','recomendacion','conclusion',];
    
        public function scopeName($query, $name){
            if(trim($name)!=""){
                $query->where('nombre', "LIKE", "%$name%");
                }
            }
        }