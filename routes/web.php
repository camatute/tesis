<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
Route::get('perfil/informacion_editar',['as'=> 'perfil.edit', 'uses' => 'UsersController@edit']);
Route::put('perfil/informacion_editar',['as'=> 'perfil.update', 'uses' => 'UsersController@update']);
Route::resource('/paciente', 'PacientesController');
route::resource('/usuarios', 'InformacionesController');
Route::resource('/tratamiento', 'TratamientosController');
Route::resource('/evaluacion', 'RegistroController');
Route::get('/tratamiento_lista', 'TratamientosController@tratamiento_lista');
Route::get('paciente/pdfexport/{paciente}', 'PacientesController@pdfexport');
Route::get('evaluacion/pdfexport/{evaluacion}', 'RegistroController@pdfexport');


// PDF

// Route::get('pdf', function() {
//     $users = App\Paciente::find($id);
//     $pdf = PDF::loadview('ejemplo', ['paciente' => $users]);
//     return $pdf->download('ejemplo.pdf');
// });

