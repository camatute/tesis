

<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    /**
     * Funcion para añadir una nueva fila en la tabla
     */
     var cont=0;

    var jsonPaciente = {!! json_encode($paciente->toArray(), JSON_HEX_TAG) !!}
    cont = jsonPaciente.miembros.length;

    $("#add").click(function() {
        console.log(cont);
        cont++;
        var nuevaFila = "<tr> \
                <input type='hidden' name='miembros[" + cont + "][id]' value=''>  \
                <td><input type='text' name='miembros[" + cont + "][m_casa]'></td> \
                <td><input type='text' name='miembros[" + cont + "][m_edad]'></td> \
                <td><input type='text' name='miembros[" + cont + "][m_sexo]'></td> \
                <td><input type='text' name='miembros[" + cont + "][m_relniño]'></td> \
				<td><input type='button' class='del' value='Eliminar Fila'></td> \
			</tr>";
        $("#tabla tbody").append(nuevaFila);
    });

    // evento para eliminar la fila
    $("#tabla").on("click", ".del", function() {
        $(this).parents("tr").remove();
    });
});
</script>

@section('title', "Centro de Salud")
@section('name', "Home")


@extends('layouts.app1')

@yield('sidebar')
@section('content')

@include('Plantilla.tabla')
<div>
    
    <div class="container">
    <br><br>
    <form action="{{ route('paciente.update', $paciente->id) }}" method="POST">
        @method('put')
        @csrf
            <h1>EDICIÓN DE PACIENTE</h1>


            @if (count($errors) > 0)


            <div class="alert alert-danger">
                <div>
                    <ul>
                        @foreach ($errors->all() as $message)
                        <h6><error>Error!</strong> {{ $message }}</h6>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif

            <div class="form-row">
                <div class="col-7">
                    <input type="text" value="{{$paciente->nombre}}" name="nombre" class="form-control" placeholder="Nombre del Paciente">
                </div>
                <div class="col">
                    <input type="date" value="{{$paciente->f_nacimiento}}" name="f_nacimiento" class="form-control" placeholder="Fecha de Nacimiento">
                </div>
                <div class="col-">
                    <input type="text" value="{{$paciente->edad}}" name="edad" class="form-control" placeholder="Edad Cronologica">
                </div>
            </div>


            <div class="form-row">
                <div class="col-7">
                    <input type="text" value="{{$paciente->direccion}}" name="direccion" class="form-control" placeholder="Dirección del Paciente">
                </div>
                <div class="col">
                    <input type="int" value="{{$paciente->telefono}}" name="telefono" class="form-control" placeholder="Teléfono del Paciente">
                </div>
                <div class="col">
                    <input type="hidden">
                </div>
            </div>

            <div class="form-row">
                <div class="col-7">
                    <input type="text" value="{{$paciente->referencia}}" name="referencia" class="form-control"
                        placeholder="Persona o Institución que le refirió el Centro">
                </div>
                <div class="col">
                    <input type="int" value="{{$paciente->motivo}}" name="motivo" class="form-control" placeholder="Motivo de la Consulta">
                </div>
            </div><br>



            <h5>PACIENTE REFERIDO AL PROBLEMA</h5>
            <div class="form-row">
                <div class="col-">
                    <input type="text" value="{{$paciente->fonacion}}" name="fonacion" class="form-control" placeholder="Fonación">
                </div>
                <div class="col-">
                    <input type="int" value="{{$paciente->lenguaje}}" name="lenguaje" class="form-control" placeholder="Lenguaje">
                </div>
                <div class="col-">
                    <input type="text" value="{{$paciente->habla}}" name="habla" class="form-control" placeholder="Habla">
                </div>
                <div class="col-">
                    <input type="int" value="{{$paciente->audicion}}" name="audicion" class="form-control" placeholder="Audición">
                </div>
                <div class="col">
                    <input type="int" value="{{$paciente->otro}}" name="otro" class="form-control" placeholder="Otro">
                </div>
            </div><br>




            <h5>DATOS DE LOS PADRES</h5>

            <div class="form-row">
                <div class="col-7">
                    <input type="text" value="{{$paciente->n_padre}}" name="n_padre" class="form-control" placeholder="Nombre del Papá">
                </div>
                <div class="col-">
                    <input type="text" value="{{$paciente->e_padre}}" name="e_padre" class="form-control" placeholder="Edad del Papá">
                </div>
                <div class="col">
                    <input type="text" value="{{$paciente->o_padre}}" name="o_padre" class="form-control" placeholder="Profesión del Papá">
                </div>
            </div>

            <div class="form-row">
                <div class="col-7">
                    <input type="text" value="{{$paciente->n_madre}}" name="n_madre" class="form-control" placeholder="Nombre del Mamá">
                </div>
                <div class="col-">
                    <input type="text" value="{{$paciente->e_madre}}" name="e_madre" class="form-control" placeholder="Edad del Mamá">
                </div>
                <div class="col">
                    <input type="text" value="{{$paciente->o_madre}}" name="o_madre" class="form-control" placeholder="Profesión del Mamá">
                </div>
            </div><br>


            <table id="tabla" class="tabla">
                <caption>Miembros de Familia</caption>
                <thead>
                    <tr>
                        <!-- <th>Cedula</th> -->
                        <th>Nombre</th>
                        <th>Edad</th>
                        <th>Sexo</th>
                        <th>Relación</th>

                        <th><input type="button" id="add" value="Añadir"></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($paciente->miembros as $miembro)
                <tr>
                    <td>
                        <input type='hidden' name='miembros[{{$loop->index }}][id]' value="{{$miembro->id}}" class="x">
                        <input  type='text' name='miembros[{{$loop->index }}][m_casa]' value="{{$miembro->m_casa}}">
                    </td>
                    <td><input  type='text' name='miembros[{{$loop->index }}][m_edad]' value="{{$miembro->m_edad}}"></td>
                    <td><input  type='text' name='miembros[{{$loop->index }}][m_sexo]' value="{{$miembro->m_sexo}}"></td>
                    <td><input  type='text' name='miembros[{{$loop->index }}][m_relniño]' value="{{$miembro->m_relniño}}"></td>
                    <td><input  type='button' class='del' value='Eliminar Fila'></td>
                </tr>
            @endforeach
                </tbody>
            </table><br>


            <div class="form-row ">
                <div class="col">
                    <input type="int" value="{{$paciente->observacion}}" name="observacion" class="form-control" placeholder="Observación">
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <input type="int" value="{{$paciente->recomendacion}}" name="recomendacion" class="form-control" placeholder="Recomendación">
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <input type="int" value="{{$paciente->conclusion}}" name="conclusion" class="form-control" placeholder="Conclusión">
                </div>
            </div>

            <div class="contenedor">
                <br>
                <div class="row justify-content-md-center">
                    <div class="col-4">
                        <input title="GUARDAR" type=image src="/images/guardar.png" width="50" height="50"
                            class="imagenpequeña">
                        <h5>Guardar</h5>

                    </div>
                    <div class="col-4">
                        <a title="CANCELAR" href="/paciente"><img src="/images/cancelar.png" class="pequeña"></a>
                        <h5>Cancelar</h5>
                    </div>
                </div>

        </form>
    </div>


</div>
@endsection