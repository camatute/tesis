

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>

<style>
h1 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
}

h2 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
}

h3 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
}

h4 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
}

p {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;

}

table {
    width: 100%;
    color: black;
    text-align: center;
    border-collapse: collapse;
    border: solid 1px #000000;
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;

}

table th {
    font-size: 13px;
    font-weight: normal;
    padding: 0px;
    color: black;
    border-top: 0px solid black;
    border-bottom: 1px solid black;

    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;

}
label{
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
line-height: 20pt;        /* para la separacion entre lineas */ 
text-indent: 30pt;        /* para sangrias */ 
}

/* Configuracion para la paginacion */
#header,
#footer {
  position: fixed;
  left: 0;
	right: 0;
	color: #aaa;
	font-size: 0.9em;
}
#header {
  top: 0;
	border-bottom: 0.1pt solid #aaa;
}
#footer {
  bottom: 0;
  border-top: 0.1pt solid #aaa;
}
.page-number:before {
  content: "Page " counter(page);
}

.left{
    float: left;
    
}

.center{
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
    font-size: 200%;
    }

img.pequeña {
    width: 30px;
    height: 30px;
    margin: auto;
    display: block;
}

#inferior{
position:absolute; /*El div será ubicado con relación a la pantalla*/
left:0px; /*A la derecha deje un espacio de 0px*/
right:0px; /*A la izquierda deje un espacio de 0px*/
bottom:100px; /*Abajo deje un espacio de 0px*/
height:0px; /*alto del div*/
z-index:0;
 }

</style>


<body>
<p>
<div class="left" id="center" class="pequeña">
<img src="{{ public_path('images/sigsig_logo.png') }}" class="pequeña">
</div>
<div class="center" id="center">
CENTRO DIURNO DE CUIDADO Y DESARROLLO INTEGRAL
</div>


<h2>REGISTRO DE PACIETNE</h2><br>


        <label>Nombre: </label>{{$paciente->nombre}}
        <label>Fecha de Nacimiento: </label>
        {{$paciente->f_nacimiento}}<br>
        <label>Edad Cronológica: </label>
        {{$paciente->edad}}
        <label>Dirección: </label>
        {{$paciente->direccion}}
        <label>Teléfono: </label>
        {{$paciente->telefono}}<br><br>
        <label> PERSONA O INTITUCION QUE LE REFIRIO AL CENTRO:</label> <br>
        {{$paciente->referencia}}<br><br>
        <label> MOTIVO DE LA CONSULTA:</label> <br>
        {{$paciente->motivo}}
        <!-- <label> PACIENTE REFERIDO CON PROBLEMA DE:</label> <br><br>
        <label>Fonacion: </label>
        {{$paciente->fonacion}}
        <label>Lenguaje: </label>
        {{$paciente->lenguaje}}
        <label>habla: </label>
        {{$paciente->habla}}
        <label>Audicion: </label>
        {{$paciente->audicion}}
        <label>OTRO: </label>
        {{$paciente->otro}}<br><br> -->

        <h4>PROBLEMA REFERIDO</h4>
        <table id="tabla">
            <thead>
                <tr>
                    <th>Fonación</th>
                    <th>Lenguaje</th>
                    <th>Habla</th>
                    <th>Audición</th>
                    <th>Otro</th>
                </tr>
            </thead>
            <tbody> 
                <tr>
                <td>{{$paciente->fonacion}}</td>
                <td>{{$paciente->lenguaje}}</td>
                <td>{{$paciente->habla}}</td>
                <td>{{$paciente->audicion}}</td>
                <td>{{$paciente->otro}}</td>
                </tr>
            </tbody>
        </table>
        


        <!-- <label>Nombre de la Madre:</label>
        {{$paciente->n_madre}}
        <label>Edad:</label>
        {{$paciente->e_madre}}
        <label>Ocupacio:</label>
        {{$paciente->o_madre}} <br><br>
        <label>Nombre del Padre:</label>
        {{$paciente->n_padre}}
        <label>Edad:</label>
        {{$paciente->e_padre}}
        <label>Ocupacio:</label>
        {{$paciente->o_padre}}<br><br> -->

        <h4>PADRES DEL PACIENTE</h4>
        <table id="tabla">
            <thead>
                <tr>
                    <th>Nombre - Apellido</th>
                    <th>Edad</th>
                    <th>Ocupación</th>
                </tr>
            </thead>
            <tbody> 
                <tr>
                    <td>{{$paciente->n_madre}}</td>
                    <td>{{$paciente->e_madre}}</td>
                    <td>{{$paciente->o_madre}}</td>
                </tr>

                <tr>
                    <td>{{$paciente->n_padre}}</td>
                    <td>{{$paciente->e_padre}}</td>
                    <td>{{$paciente->o_padre}}</td>
                </tr>
            </tbody>
        </table>

        

        <h4>MIEMBROS</h4>
        <table id="tabla">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Edad</th>
                    <th>Sexo</th>
                    <th>Relación</th>

                </tr>
            </thead>
            <tbody> 
            @foreach($paciente->miembros as $miembro)
                <tr>
                    <td>{{$miembro->m_casa}}</td>
                    <td>{{$miembro->m_edad}}</td>
                    <td>{{$miembro->m_sexo}}</td>
                    <td>{{$miembro->m_relniño}}</td>
                </tr>
            @endforeach
            </tbody>
        </table><br>

        <label>Observación:</label>
        {{$paciente->observacion}}<br>
        <label>Recomendación:</label>
        {{$paciente->recomendacion}}<br>
        <label>Conclusión:</label>
        {{$paciente->conclusion}}<br><br><br>


       
        

        <div id="inferior">
        <h4>___________________________</h4>
        <h4>{{ Auth::user()->name }}</h4>
        </div>

</p>

{{-- Here's the magic. This MUST be inside body tag. Page count / total, centered at bottom of page --}}
<script type="text/php">
    if (isset($pdf)) {
        $text = "page {PAGE_NUM} / {PAGE_COUNT}";
        $size = 10;
        $font = $fontMetrics->getFont("Verdana");
        $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
        $x = ($pdf->get_width() - $width) / 2;
        $y = $pdf->get_height() - 35;
        $pdf->page_text($x, $y, $text, $font, $size);
    }
</script>
</body>