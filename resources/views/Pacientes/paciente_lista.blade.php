@section('title', "Centro de Salud")
@section('name', "Home")

@extends('layouts.app1')
@yield('sidebar')
@section('content')
<style>

</style>
<div class="container">
    <br><br>
    <h1>LISTADO DE PACIENTES</h1>


    <a title="Nuevo Paciente" href="{{ route('paciente.create')}}"><img src="/images/nuevo1.png" class="imagenpequeña1"></a>
    <!-- <h5>Crear Paciente</h5> -->

    <h5>
        <a title="Nuevo Paciente" href="{{ route('paciente.create')}}">Crear Paciente</a>
    </h5>

    <form>
        <div class="card-body row no-gutters align-items-center">
            <!--e   nd of col-->
            <div class="col">
                <input class="form-control form-control-lg form-control-borderless" type="search"
                    placeholder="Buscar" name="search" id="search">
            </div>
            <!--end of col-->
            <div class="col-auto">
                <button class="btn btn-lg btn-success" type="submit">Buscar</button>
            </div>
            <!--end of col-->
        </div>
    </form>

</div><br>
<div class="table-responsive1">
    <table class="table small">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Edad</th>
                <th>Teléfono</th>
                <th>Motivo</th>
                <th>Opciones</th>
            </tr>
        </thead>
        @forelse($pacientes as $paciente)
        <tr>
            <td>{{ $paciente->id }}</td>
            <td>{{ $paciente->nombre }}</td>
            <td>{{ $paciente->edad }}</td>
            <td>{{ $paciente->telefono }}</td>
            <td>{{ $paciente->motivo }}</td>
            <td>
                <div class="container">

                    <div class="row justify-content-md-center">
                        <div class="col-">
                            <a title="Editar" href="{{route('paciente.edit', $paciente->id)}}"><img
                                    src="/images/editar.png" class="pequeña"></a>
                        </div>
                        <div class="col-">
                            <a title="PDF" href="{{ url('paciente/pdfexport/' . $paciente->id)}}" target="_blank"><img
                                    src="/images/pdf.png" class="pequeña"></a>
                        </div>
                        <div class="col-">
                        
                            <form action="{{route('paciente.destroy', $paciente->id)}}" method="post">
                                @method('delete')
                                @csrf
                                
                                <input title="Eliminar" type=image src="/images/eliminar.png" width="50" height="50"
                                    class="imagenpequeña"> </form>
                        </div>
                    </div>
                </div>
            </td>


        </tr>

        @empty


        <h5>No existe pacientes registrados</h5>

        @endforelse


    </table>
    <div class="paginate">
    {!!$pacientes->render()!!}

    </div>

</div>



</div>
@endsection