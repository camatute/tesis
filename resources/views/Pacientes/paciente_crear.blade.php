<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    /**
     * Funcion para añadir una nueva fila en la tabla
     */
    var cont = 0;
    $("#add").click(function() {
        cont++;
        var nuevaFila = "<tr> \
			  //<td><input type='text' name='miembros[" + cont + "][m_casa]'></td> \
                <td><input type='text' name='miembros[" + cont + "][m_edad]'></td> \
                <td><input type='text' name='miembros[" + cont + "][m_sexo]'></td> \
                <td><input type='text' name='miembros[" + cont + "][m_relniño]'></td> \
				<td><input type='button' class='del' value='Eliminar'></td> \
			</tr>";
        $("#tabla tbody").append(nuevaFila);
    });

    // evento para eliminar la fila
    $("#tabla").on("click", ".del", function() {
        $(this).parents("tr").remove();
    });

    $("#add").click();
});
</script>

<style>
.error {
    color: #D8000C;
    background-color: #FFBABA;
    background-image: url('imagenes/error.jpg');
}
h5{

}

</style>
@section('title', "Centro de Salud")
@section('name', "Home")


@extends('layouts.app1')

@yield('sidebar')
@section('content')

@include('Plantilla.tabla')
<div>
    <br><br>
    <div class="container">
        <form action="{{ route('paciente.store') }}" method="POST">
            @method('post')
            @csrf
            <h1>REGISTRO DE PACIENTE</h1>


            
            @if (count($errors) > 0)


            <div class="alert alert-danger">
                <div>
                    <ul>
                        @foreach ($errors->all() as $message)
                        <h6><error>Error!</strong> {{ $message }}</h6>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif

            <div class="form-row">
                <div class="col-7">

                    <input type="text" name="nombre" class="form-control" placeholder="Nombre del Paciente">
                </div>
                <div class="col">
                    <input type="date" name="f_nacimiento" class="form-control" placeholder="Fecha de Nacimiento">
                </div>
                <div class="col-">
                    <input type="text" name="edad" class="form-control" placeholder="Edad Cronológica">
                </div>
            </div>


            <div class="form-row">
                <div class="col-7">
                    <input type="text" name="direccion" class="form-control" placeholder="Dirección del Paciente">
                </div>
                <div class="col">
                    <input type="int" name="telefono" class="form-control" placeholder="Teléfono del Paciente">
                </div>
                <div class="col">
                    <input type="hidden">
                </div>
            </div>

            <div class="form-row">
                <div class="col-7">
                    <input type="text" name="referencia" class="form-control"
                        placeholder="Persona o Institución que le refirió al Centro">
                </div>
                <div class="col">
                    <input type="int" name="motivo" class="form-control" placeholder="Motivo de la Consulta">
                </div>
            </div><br>



            <h5>PACIENTE REFERIDO AL PROBLEMA</h5>
            <div class="form-row">
                <div class="col-">
                    <input type="text" name="fonacion" class="form-control" placeholder="Fonación">
                </div>
                <div class="col-">
                    <input type="int" name="lenguaje" class="form-control" placeholder="Lenguaje">
                </div>
                <div class="col-">
                    <input type="text" name="habla" class="form-control" placeholder="Habla">
                </div>
                <div class="col-">
                    <input type="int" name="audicion" class="form-control" placeholder="Audición">
                </div>
                <div class="col">
                    <input type="int" name="otro" class="form-control" placeholder="Otro">
                </div>
            </div><br>




            <h5>DATOS DE LOS PADRES</h5>

            <div class="form-row">
                <div class="col-7">
                    <input type="text" name="n_padre" class="form-control" placeholder="Nombre del Papá">
                </div>
                <div class="col-">
                    <input type="text" name="e_padre" class="form-control" placeholder="Edad del Papá">
                </div>
                <div class="col">
                    <input type="text" name="o_padre" class="form-control" placeholder="Profesión del Papá">
                </div>
            </div>

            <div class="form-row">
                <div class="col-7">
                    <input type="text" name="n_madre" class="form-control" placeholder="Nombre de la Mamá">
                </div>
                <div class="col-">
                    <input type="text" name="e_madre" class="form-control" placeholder="Edad de la Mamá">
                </div>
                <div class="col">
                    <input type="text" name="o_madre" class="form-control" placeholder="Profesión de la Mamá">
                </div>
            </div><br>


            <table id="tabla" class="tabla">
                <caption>Miembros de Familia</caption>
                <thead>
                    <tr>
                        <!-- <th>Cedula</th> -->
                        <th>Nombre</th>
                        <th>Edad</th>
                        <th>Sexo</th>
                        <th>Relación</th>

                        <th><input type="button" id="add" value="Añadir"></th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table><br>


            <div class="form-row">
                <div class="col">
                    <input type="int" name="observacion" class="form-control" placeholder="Observación">
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <input type="int" name="recomendacion" class="form-control" placeholder="Recomendación">
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <input type="int" name="conclusion" class="form-control" placeholder="Conclusión">
                </div>
            </div>
            
            <div class="contenedor">
                <br>
                <div class="row justify-content-md-center">
                    <div class="col-4">
                        <input title="GUARDAR" type=image src="/images/guardar.png" width="50" height="50"
                            class="imagenpequeña">
                        <h5>Guardar</h5>

                    </div>
                    <div class="col-4">
                        <a title="CANCELAR" href="/paciente"><img src="/images/cancelar.png" class="pequeña"></a>
                        <h5>Cancelar</h5>
                    </div>
                </div>
        </form>
    </div>
</div>



</div>
@endsection