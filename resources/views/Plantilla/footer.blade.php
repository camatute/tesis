<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <style>
    .social-sharing {
        display: block;
        position: fixed;
        width: 100%;
        left: 0;
        bottom: 0;

    }

    .social-sharing ul.menu-social {
        list-style: none;
        text-align: center;
        margin: 0;
        padding: 0;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-wrap: nowrap;
        -ms-flex-wrap: nowrapwrap;
        flex-wrap: nowrap;
        padding-right: 10px;
    }

    .social-sharing ul.menu-social li {
        flex: 0 1 40px;
        height: 40px;
    }

    .social-sharing ul.menu-social li.newsletter {
        flex: 1 1 auto;
        line-height: 40px;
        position: relative;
        background-color: #fafafa;
        background-image: repeating-linear-gradient(135deg, #ffe4e1 0px, #ffe4e1 5px, transparent 5px, transparent 10px, #e1f3ff 10px, #e1f3ff 15px, transparent 15px, transparent 20px);
    }

    .social-sharing ul.menu-social li.copyright {
        flex: 1 1 auto;
        font-size: 15px;
        font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;

    }

    ul.menu-social li a::before {
        content: "©";
        display: inline-block;
        font-family: FontAwesome;
        font-size: 20px;
        vertical-align: middle;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        line-height: 40px;

    }

    ul.menu-social li a[href*="facebook.com"]::before {
        content: "\f09a";
    }

    ul.menu-social li a[href*="twitter.com"]::before {
        content: "\f099";
    }

    ul.menu-social li a[href*="pinterest.com"]::before {
        content: "\f231";
    }

    ul.menu-social li a[href*="whatsapp://send"]::before {
        content: "\f232";
    }
   
    </style>
</head>

<!-- <footer>
<div class="container">
        <div class="text-center center-block">
           
                <a href=""><i width:6; height:6; id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
                <a href=""><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                <a href=""><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
                <a href=""><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>

</div>
</div>
</footer> -->

<footer class="social-sharing">
    <ul class="menu-social">

        <li class="social-item copyright"><a></a></li>
        <li class="social-item"><a
                href="https://www.facebook.com/desarrollosocialmunicipaldesigsig/"
                target="_blank"><span class="screen-reader-text"></span></a></li>
        <li class="social-item"><a
                href="https://twitter.com/gadsigsig?lang=es"
                target="_blank"><span class="screen-reader-text"></span></a></li>
        <!-- <li class="social-item"><a href="https://www.pinterest.com/pin/find/?url=https://www.silocreativo.com/en/" target="_blank"><span class="screen-reader-text">Pinterest</span></a></li>
		<li class="social-item"><a href="whatsapp://send?text=https://www.silocreativo.com/en/" data-action="share/whatsapp/share"><span class="screen-reader-text">Whatsapp</span></a></li>
   -->
    </ul>
</footer>

</html>