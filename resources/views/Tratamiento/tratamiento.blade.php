@section('title', "Centro de Salud")
@section('name', "Home")

@extends('layouts.app1')
@yield('sidebar')
@section('content')

<div class="container">
    <br><br>
    <h1>TRATAMIENTOS</h1>

    <form>
        <div class="card-body row no-gutters align-items-center">
            <!--end of col-->
            <div class="col">
                <input class="form-control form-control-lg form-control-borderless" type="search" placeholder="Buscar"
                    name="search" id="search">
            </div>
            <!--end of col-->
            <div class="col-auto">
                <button class="btn btn-lg btn-success" type="submit">Buscar</button>
            </div>
            <!--end of col-->
        </div>
    </form>

    <div class="row">
        @foreach($tratamientos as $tratamiento)
        <div class="col-sm">
            <div class="card" style="width: 20rem; height:">
                <img src="imagen/{{$tratamiento->imagen}}" class="card-img-top" alt="..." width="304" height="236">
                <div class="card-body">
                    <h5 class="card-title" >{{$tratamiento->palabra}}</h5>
                    <p class="card-text"></p>
                    <audio controls="controls">
                        <source src="sound/{{$tratamiento->audio}}" type="audio/ogg" />
                        <source src="sound/{{$tratamiento->audio}}" type="audio/mpeg" />
                    </audio>
                </div>
               
            </div>
            <br>
        </div>
        @endforeach

    </div>
    <div class="paginate">
    {{$tratamientos->links()}}
    </div>

</div>
@endsection