@section('title', "Centro de Salud")
@section('name', "Home")

@extends('layouts.app1')
@yield('sidebar')
@section('content')
<div class="container">
    <br><br>
    <h1>MATERIAL DE TRATAMIENTO</h1>


    <a title="Nuevo Tratamiento" href="tratamiento/create"><img src="/images/tratamiento_nuevo.svg" class="imagenpequeña1"></a>
    <!-- <h5>Crear</h5> -->

    <h5>
        <a title="Nuevo Tratamiento" href="tratamiento/create">Crear Tratamiento</a>
    </h5>

    <form>
        <div class="card-body row no-gutters align-items-center">
            <!--end of col-->
            <div class="col">
                <input class="form-control form-control-lg form-control-borderless" type="search"
                    placeholder="Buscar" name="search" id="search">
            </div>
            <!--end of col-->
            <div class="col-auto">
                <button class="btn btn-lg btn-success" type="submit">Buscar</button>
            </div>
            <!--end of col-->
        </div>
    </form>

</div>
<div class="table-responsive1">
    <table class="table small">
        <thead>
            <tr class="table-secondary">
            <th>Id</th>
            <th>Imagen</th>
            <th>Palabra</th>
            <th>Audio</th>
            <th>Opciones</th>
            </tr>
        </thead>
        @forelse($tratamientos as $tratamiento)
        <tr>
            <td>{{ $tratamiento->id }}</td>
            <td>{{ $tratamiento->imagen }}</td>
            <td>{{ $tratamiento->palabra }}</td>
            <td>{{ $tratamiento->audio }}</td>
            <td>
                <div class="container">

                    <div class="row justify-content-md-center">
                        <div class="col-">
                        <a title="Editar" href="{{route('tratamiento.edit', $tratamiento->id)}}"><img
                                src="/images/editar.png" class="pequeña"></a>
                        </div>
                        <div class="col-">
                        <form action="{{route('tratamiento.destroy', $tratamiento->id)}}" method="post">
                                @method('delete')
                                @csrf
                                <input title="Eliminar" type=image src="/images/eliminar.png" class="imagenpequeña">
                            </form>
                        </div>
                    </div>
                </div>
            </td>


        </tr>

        @empty


        <h5>No existe tratamientos registrados</h5>

        @endforelse


    </table>
    <div class="paginate">
    {{$tratamientos->render()}}
    </div>

</div>



</div>
@endsection