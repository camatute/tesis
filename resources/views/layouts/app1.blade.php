<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />

</head>
<style>
.sidenav {

    height: 100%;
    width: 200px;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #212529;
    overflow-x: hidden;
    padding-top: fixed;
}

.sidenav a {
    padding: 6px 8px 6px 16px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
}

.sidenav a:hover {
    color: #f1f1f1;
}

.main {
    margin-left: 200px;
    /* Same as the width of the sidenav */
    font-size: 28px;
    /* Increased text to enable scrolling */
    padding: 0px 10px;
}

@media screen and (max-height: 450px) {
    .sidenav {
        padding-top: 15px;
    }

    .sidenav a {
        font-size: 18px;
    }
}


/* IMAGENES */

img.pequeña {
    width: 30px;
    height: 29px;
    margin: auto;
    display: block;
    /* opacity:0.7; */
}

.imagenpequeña {
    width: 30px;
    height: 30px;
    margin: auto;
    display: block;
    padding:2px;
    /* opacity:0.7; */
}

.imagenpequeña1 {
    width: 40px;
    height: 40px;
    margin: auto;
    display: block;
    /* filter: drop-shadow(5px 5px 10px #444); */
    /* opacity:0.7; */
}


img.mediana {
    width: 100px;
    height: 100px;
    margin: auto;
    display: block;
    filter: drop-shadow(5px 5px 10px #444);
}

img.grande {
    width: 100%;
    height: 127px;
    margin: auto;
    display: block;
}

/* Estilo de Letras */
h1 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
    size:14px;
}

h2 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
}

h3 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
}

h4 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
}

h5 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
}

p {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;

}

/* Tabla */
.table{
    width: 100%;
}
.table-responsive1 {
    width: 80%;
    margin-left: 10%;
    margin-right: 10%;
    padding: 2%;
    background: white;
}

table th {
    text-align: center;
    font-size: 13px;
    font-weight: normal;
    padding: 8px;
    color:white;
    background: #3490dc !important;
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;

}

table td {
    text-align: center;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 13px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 18.5714px;

}

/* INPUTS */

.input1 {
    width: 20%;
    height: normal;
    font: normal Helvetica;
}

.input2 {
    width: 92%;
    height: normal;
    font: normal Helvetica;
}

.input3 {
    width: 40%;
    height: normal;
    font: normal Helvetica;
}

.input4 {
    width: 32%;
    height: normal;
    font: normal Helvetica;
}

.input5 {
    width: 36%;
    height: normal;
    
}
/* Paginacion */
.paginate {
    font-size: 13px;
    font-weight: normal;
    padding-left: 45%;
    padding-right: 45%;
    
}


/* Contendor Formulario */


</style>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-primary">
            <a class="navbar-brand " href="{{ url('/home') }}">
                @yield('name')
            </a>
            <div class="container">

                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    <div class="sidenav">
        @section('sidebar')
        @if(Auth::user()->rol == 1)
        @include('Plantilla.asideAdmin')
        @elseif(Auth::user()->rol == 2)
        @include('Plantilla.aside')
        @endif
        @show
    </div>

    <div class="main">
        @yield('content')
    </div>


   

    </div>

</body>

</html>