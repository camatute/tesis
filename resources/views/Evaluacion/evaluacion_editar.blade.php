<script src="http://code.jquery.com/jquery-latest.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    /**
     * Funcion para añadir una nueva fila en la tabla
     */
    var cont = 0;

    var jsonEvaluacion = {
        !!json_encode($evaluacion - > toArray(), JSON_HEX_TAG) !!
    }
    cont = jsonEvaluacion.palabras.length;

    $("#add").click(function() {
        cont++;
        var nuevaFila = "<tr> \
              //<input type='hidden' name='palabras[" + cont + "][id]' value=''>  \
                <td><input type='text' name='palabras[" + cont + "][estimulo]'></td> \
                <td><input type='text' name='palabras[" + cont + "][fonema]'></td> \
                <td><input type='text' name='palabras[" + cont + "][inicial]'></td> \
                <td><input type='text' name='palabras[" + cont + "][media]'></td> \
                <td><input type='text' name='palabras[" + cont + "][final]'></td> \
                <td><input type='text' name='palabras[" + cont + "][transicion]'></td> \
                <td><input type='text' name='palabras[" + cont + "][respuesta]'></td> \
				<td><input type='button' class='del' value='Eliminar Fila'></td> \
			</tr>";
        $("#tabla tbody").append(nuevaFila);
    });

    // evento para eliminar la fila
    $("#tabla").on("click", ".del", function() {
        $(this).parents("tr").remove();
    });

});
</script>

<script type="text/javascript">
$(document).ready(function() {
    /**
     * Funcion para añadir una nueva fila en la tabla
     */
    var cont = 0;
    var jsonEvaluacion = {
        !!json_encode($evaluacion - > toArray(), JSON_HEX_TAG) !!
    }
    cont = jsonEvaluacion.diptongos.length;
    $("#add1").click(function() {
        cont++;
        var nuevaFila = "<tr> \
              //<input type='hidden' name='diptongos[" + cont + "][id]' value=''>  \
              <td><input type='text' name='diptongos[" + cont + "][diptongo]'></td> \
                <td><input type='text' name='diptongos[" + cont + "][estimulod]'></td> \
                <td><input type='text' name='diptongos[" + cont + "][respuestad]'></td> \
                <td><input type='text' name='diptongos[" + cont + "][transiciond]'></td> \
				<td><input type='button' class='del' value='Eliminar Fila'></td> \
			</tr>";
        $("#tabla1 tbody").append(nuevaFila);
    });

    // evento para eliminar la fila
    $("#tabla1").on("click", ".del", function() {
        $(this).parents("tr").remove();
    });

});
</script>
<script type="text/javascript">
$(document).ready(function() {
    /**
     * Funcion para añadir una nueva fila en la tabla
     */
    var cont = 0;
    var jsonEvaluacion = {
        !!json_encode($evaluacion - > toArray(), JSON_HEX_TAG) !!
    }
    cont = jsonEvaluacion.difonos.length;
    $("#add2").click(function() {
        cont++;
        var nuevaFila = "<tr> \
              //<input type='hidden' name='difonos[" + cont + "][id]' value=''>  \
                <td><input type='text' name='difonos[" + cont + "][difonos]'></td> \
                <td><input type='text' name='difonos[" + cont + "][estimulodf]'></td> \
                <td><input type='text' name='difonos[" + cont + "][respuestadf]'></td> \
                <td><input type='text' name='difonos[" + cont + "][transiciondf]'></td> \
				<td><input type='button' class='del' value='Eliminar Fila'></td> \
			</tr>";
        $("#tabla2 tbody").append(nuevaFila);
    });

    // evento para eliminar la fila
    $("#tabla2").on("click", ".del", function() {
        $(this).parents("tr").remove();
    });

});
</script>

<script type="text/javascript">
$(document).ready(function() {
    /**
     * Funcion para añadir una nueva fila en la tabla
     */
    var cont = 0;
    var jsonEvaluacion = {
        !!json_encode($evaluacion - > toArray(), JSON_HEX_TAG) !!
    }
    cont = jsonEvaluacion.silabasinversas.length;
    $("#add3").click(function() {
        cont++;
        var nuevaFila = "<tr> \
              //<input type='hidden' name='silabasinversas[" + cont + "][id]' value=''>  \
                <td><input type='text' name='silabasinversas[" + cont + "][silabas]'></td> \
                <td><input type='text' name='silabasinversas[" + cont + "][estimulods]'></td> \
                <td><input type='text' name='silabasinversas[" + cont + "][respuestads]'></td> \
                <td><input type='text' name='silabasinversas[" + cont + "][transicionds]'></td> \
				<td><input type='button' class='del' value='Eliminar Fila'></td> \
			</tr>";
        $("#tabla3 tbody").append(nuevaFila);
    });

    // evento para eliminar la fila
    $("#tabla3").on("click", ".del", function() {
        $(this).parents("tr").remove();
    });

});
</script>
<style>
table {
    margin-left: 0%;
    width: 100%;
    color: black;
    text-align: center;
    border-collapse: collapse;
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;

}

table th {
    font-size: 13px;
    font-weight: normal;
    padding: 8px;
    color: white;
    background: #3490dc !important;

    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;

}

input {
    border-radius: 2px;
    outline: none;
    border: 1px solid #ccc;
    width: 100%;
}

table td {
    text-align: center;
    opacity: 0.8;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 13px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 18.5714px;

}
</style>

@section('title', "Centro de Salud")
@section('name', "Home")


@extends('layouts.app1')

@yield('sidebar')
@section('content')

<div class="container">
    <br><br>

        <form action="{{ route('evaluacion.update', $evaluacion->id) }}" method="POST">
            @method('put')
            @csrf
            <h1>EDICIÓN DE EVALUACIÓN</h1>


            @if (count($errors) > 0)


            <div class="alert alert-danger">
                <div>
                    <ul>
                        @foreach ($errors->all() as $message)
                        <h6><error>Error!</strong> {{ $message }}</h6>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif

            <h6>Su Paciente es: {{$pacientes->nombre}}</h6>
            
            <div class="form-row">
            <div class="col">
                <h4>Paciente </h4>
                <select class="form-control" name="id_p">
                    <option value="">--NINGUNO--</option>
                    @foreach ($groups as $group)
                    <option value="{{$group->id}}">{{ $group->nombre }}</option>
                    @endforeach
                </select>
                </div>
                <div class="col">
                <h4>Fecha de Evaluación</h4>
                <input type="date" value="{{$evaluacion->fecha}}" name="fecha" class="form-control" placeholder="Fecha de Evaluacion">
                </div>
            </div><br>


            <table id="tabla">
                <thead>
                    <tr>
                        <!-- <th>Cedula</th> -->
                        <th>Estimulo</th>
                        <th>Fonema</th>
                        <th>Inicial</th>
                        <th>Media</th>
                        <th>Final</th>
                        <th>Transcripción</th>
                        <th>Respuesta</th>

                        <th><input type="button" id="add" value="añadir fila"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($evaluacion->palabras as $palabra)

                    <tr>
                        <td>
                            <input type='hidden' name='palabras[{{$loop->index }}][id]' value="{{$palabra->id}}">
                            <input type='text' name='palabras[{{$loop->index }}][estimulo]'
                                value="{{$palabra->estimulo}}">
                        </td>
                        <td><input type='text' name='palabras[{{$loop->index }}][fonema]' value="{{$palabra->fonema}}">
                        </td>
                        <td><input type='text' name='palabras[{{$loop->index }}][inicial]'
                                value="{{$palabra->inicial}}"></td>
                        <td><input type='text' name='palabras[{{$loop->index }}][media]' value="{{$palabra->media}}">
                        </td>
                        <td><input type='text' name='palabras[{{$loop->index }}][final]' value="{{$palabra->final}}">
                        </td>
                        <td><input type='text' name='palabras[{{$loop->index }}][transicion]'
                                value="{{$palabra->transicion}}"></td>
                        <td><input type='text' name='palabras[{{$loop->index }}][respuesta]'
                                value="{{$palabra->respuesta}}"></td>
                        <td><input type='button' class='del' value='Eliminar Fila'></td>
                    </tr>
                    @endforeach

                </tbody>
            </table><br>







            <table id="tabla1">
                <thead>
                    <tr>
                        <!-- <th>Cedula</th> -->
                        <th>Diptongos</th>
                        <th>Estimulo</th>
                        <th>Respuesta</th>
                        <th>Transcripción</th>

                        <th><input type="button" id="add1" value="añadir fila"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($evaluacion->diptongos as $diptongo)
                    <tr>
                        <td>
                            <input type='hidden' name='diptongos[{{$loop->index }}][id]' value="{{$diptongo->id}}">
                            <input type='text' name='diptongos[{{$loop->index }}][diptongo]'
                                value="{{$diptongo->diptongo}}">
                        </td>
                        <td><input type='text' name='diptongos[{{$loop->index }}][estimulod]'
                                value="{{$diptongo->estimulod}}"></td>
                        <td><input type='text' name='diptongos[{{$loop->index }}][respuestad]'
                                value="{{$diptongo->respuestad}}"></td>
                        <td><input type='text' name='diptongos[{{$loop->index }}][transiciond]'
                                value="{{$diptongo->transiciond}}"></td>
                        <td><input type='button' class='del' value='Eliminar Fila'></td>
                    </tr>
                    @endforeach



                </tbody>
            </table><br>



            <table id="tabla2">
                <thead>
                    <tr>
                        <!-- <th>Cedula</th> -->
                        <th>Difonos</th>
                        <th>Estimulo</th>
                        <th>Respuesta</th>
                        <th>Transcripción</th>

                        <th><input type="button" id="add2" value="añadir fila"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($evaluacion->difonos as $difono)
                    <tr>
                        <td>
                            <input type='hidden' name='difonos[{{$loop->index }}][id]' value="{{$difono->id}}">
                            <input type='text' name='difonos[{{$loop->index }}][difonos]' value="{{$difono->difonos}}">
                        </td>
                        <td><input type='text' name='difonos[{{$loop->index }}][estimulodf]'
                                value="{{$difono->estimulodf}}"></td>
                        <td><input type='text' name='difonos[{{$loop->index }}][respuestadf]'
                                value="{{$difono->respuestadf}}"></td>
                        <td><input type='text' name='difonos[{{$loop->index }}][transiciondf]'
                                value="{{$difono->transiciondf}}"></td>
                        <td><input type='button' class='del' value='Eliminar Fila'></td>
                    </tr>
                    @endforeach
                </tbody>
            </table><br>


            <table id="tabla3">
                <thead>
                    <tr>
                        <!-- <th>Cedula</th> -->
                        <th>Inversas</th>
                        <th>Estimulo</th>
                        <th>Respuesta</th>
                        <th>Transcripción</th>

                        <th><input type="button" id="add3" value="añadir fila"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($evaluacion->silabasinversas as $silabasinversa)
                    <tr>
                        <td>
                            <input type='hidden' name='silabasinversas[{{$loop->index }}][id]'
                                value="{{$silabasinversa->id}}">
                            <input type='text' name='silabasinversas[{{$loop->index }}][silabas]'
                                value="{{$silabasinversa->silabas}}">
                        </td>
                        <td><input type='text' name='silabasinversas[{{$loop->index }}][estimulods]'
                                value="{{$silabasinversa->estimulods}}"></td>
                        <td><input type='text' name='silabasinversas[{{$loop->index }}][respuestads]'
                                value="{{$silabasinversa->respuestads}}"></td>
                        <td><input type='text' name='silabasinversas[{{$loop->index }}][transicionds]'
                                value="{{$silabasinversa->transicionds}}"></td>
                        <td><input type='button' class='del' value='Eliminar Fila'></td>
                    </tr>
                    @endforeach
                </tbody>
            </table><br>

            <div class="form-row">
                <div class="col">
                    <input type="int" value="{{$evaluacion->observacion}}" name="observacion" class="form-control"
                        placeholder="Observación">
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <input type="int" value="{{$evaluacion->recomendacion}}" name="recomendacion" class="form-control"
                        placeholder="Recomendación">
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <input type="int" value="{{$evaluacion->conclusion}}" name="conclusion" class="form-control"
                        placeholder="Conclusión">
                </div>
            </div>

            <div class="contenedor">
                <br>
                <div class="row justify-content-md-center">
                    <div class="col-4">
                        <input title="GUARDAR" type=image src="/images/guardar.png" width="50" height="50"
                            class="imagenpequeña">
                        <h5>Guardar</h5>

                    </div>
                    <div class="col-4">
                        <a title="CANCELAR" href="/evaluacion"><img src="/images/cancelar.png" class="pequeña"></a>
                        <h5>Cancelar</h5>
                    </div>
                </div>
        </form>

</div>
@endsection