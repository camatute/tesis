<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    /**
     * Funcion para añadir una nueva fila en la tabla
     */
    var cont = 0;
    $("#add").click(function() {
        cont++;
        var nuevaFila = "<tr> \
			  //<td><input type='text' name='palabras[" + cont + "][estimulo]'></td> \
                <td><input type='text' name='palabras[" + cont + "][fonema]'></td> \
                <td><input type='text' name='palabras[" + cont + "][inicial]'></td> \
                <td><input type='text' name='palabras[" + cont + "][media]'></td> \
                <td><input type='text' name='palabras[" + cont + "][final]'></td> \
                <td><input type='text' name='palabras[" + cont + "][transicion]'></td> \
                <td><input type='text' name='palabras[" + cont + "][respuesta]'></td> \
				<td><input type='button' class='del' value='Eliminar Fila'></td> \
			</tr>";
        $("#tabla tbody").append(nuevaFila);
    });

    // evento para eliminar la fila
    $("#tabla").on("click", ".del", function() {
        $(this).parents("tr").remove();
    });

    $("#add").click();
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    /**
     * Funcion para añadir una nueva fila en la tabla
     */
    var cont = 0;
    $("#add1").click(function() {
        cont++;
        var nuevaFila = "<tr> \
			  //<td><input type='text' name='diptongos[" + cont + "][diptongo]'></td> \
                <td><input type='text' name='diptongos[" + cont + "][estimulod]'></td> \
                <td><input type='text' name='diptongos[" + cont + "][respuestad]'></td> \
                <td><input type='text' name='diptongos[" + cont + "][transiciond]'></td> \
				<td><input type='button' class='del' value='Eliminar Fila'></td> \
			</tr>";
        $("#tabla1 tbody").append(nuevaFila);
    });

    // evento para eliminar la fila
    $("#tabla1").on("click", ".del", function() {
        $(this).parents("tr").remove();
    });

    $("#add1").click();
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    /**
     * Funcion para añadir una nueva fila en la tabla
     */
    var cont = 0;
    $("#add2").click(function() {
        cont++;
        var nuevaFila = "<tr> \
			  //<td><input type='text' name='difonos[" + cont + "][difonos]'></td> \
                <td><input type='text' name='difonos[" + cont + "][estimulodf]'></td> \
                <td><input type='text' name='difonos[" + cont + "][respuestadf]'></td> \
                <td><input type='text' name='difonos[" + cont + "][transiciondf]'></td> \
				<td><input type='button' class='del' value='Eliminar Fila'></td> \
			</tr>";
        $("#tabla2 tbody").append(nuevaFila);
    });

    // evento para eliminar la fila
    $("#tabla2").on("click", ".del", function() {
        $(this).parents("tr").remove();
    });

    $("#add2").click();
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    /**
     * Funcion para añadir una nueva fila en la tabla
     */
    var cont = 0;
    $("#add3").click(function() {
        cont++;
        var nuevaFila = "<tr> \
			  //<td><input type='text' name='silabas[" + cont + "][silabas]'></td> \
                <td><input type='text' name='silabas[" + cont + "][estimulods]'></td> \
                <td><input type='text' name='silabas[" + cont + "][respuestads]'></td> \
                <td><input type='text' name='silabas[" + cont + "][transicionds]'></td> \
				<td><input type='button' class='del' value='Eliminar Fila'></td> \
			</tr>";
        $("#tabla3 tbody").append(nuevaFila);
    });

    // evento para eliminar la fila
    $("#tabla3").on("click", ".del", function() {
        $(this).parents("tr").remove();
    });

    $("#add3").click();
});
</script>
<style>
table {
    margin-left: 0%;
    width: 100%;
    color: black;
    text-align: center;
    border-collapse: collapse;
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;

}

table th {
    font-size: 13px;
    font-weight: normal;
    padding: 8px;
    color: white;
    background: #3490dc !important;
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;

}
input {
        border-radius: 2px;
        outline: none;
        border: 1px solid #ccc;
        width: 100%;
    }

table td {
    text-align: center;
    opacity: 0.8;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 13px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 18.5714px;

}

</style>

@section('title', "Centro de Salud")
@section('name', "Home")


@extends('layouts.app1')

@yield('sidebar')
@section('content')

<div class="container">
    <br><br>
    
        <form action="{{ route('evaluacion.store')}}" method="POST">
            @method('post')
            @csrf
            <h1>REGISTRO DE EVALUACIÓN</h1>



            @if (count($errors) > 0)


            <div class="alert alert-danger">
                <div>
                    <ul>
                        @foreach ($errors->all() as $message)
                        <h6><error>Error!</strong> {{ $message }}</h6>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
            
            <div class="form-row">
            <div class="col">
                <h4>Paciente </h4>
                <select class="form-control" name="id_p">
                    <option value="">--NINGUNO--</option>
                    @foreach ($groups as $group)
                    <option value="{{$group->id}}">{{ $group->nombre }}</option>
                    @endforeach
                </select>
                </div>
                <div class="col">
                <h4>Fecha de Evaluación</h4>
                <input type="date" name="fecha" class="form-control" placeholder="Fecha de Evaluacion">
                </div>
            </div><br>

        <table id="tabla">
            <thead>
                <tr>
                    <!-- <th>Cedula</th> -->
                    <th>Estímulo</th>
                    <th>Fonema</th>
                    <th>Inicial</th>
                    <th>Media</th>
                    <th>Final</th>
                    <th>Transcripción</th>
                    <th>Respuesta</th>

                    <th><input type="button" id="add" value="añadir fila"></th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table><br>
        <table id="tabla1">
            <thead>
                <tr>
                    <!-- <th>Cedula</th> -->
                    <th>Diptongos</th>
                    <th>Estímulo</th>
                    <th>Respuesta</th>
                    <th>Transcripción</th>

                    <th><input type="button" id="add1" value="añadir fila"></th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table><br>
        <table id="tabla2">
            <thead>
                <tr>
                    <!-- <th>Cedula</th> -->
                    <th>Dífonos</th>
                    <th>Estímulo</th>
                    <th>Respuesta</th>
                    <th>Transcripción</th>

                    <th><input type="button" id="add2" value="añadir fila"></th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table><br>
        <table id="tabla3">
            <thead>
                <tr>
                    <!-- <th>Cedula</th> -->
                    <th>Inversas</th>
                    <th>Estímulo</th>
                    <th>Respuesta</th>
                    <th>Transcripcion</th>

                    <th><input type="button" id="add3" value="añadir fila"></th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table><br>


            <div class="form-row">
                <div class="col">
                    <input type="int" name="observacion" class="form-control" placeholder="Observación">
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <input type="int" name="recomendacion" class="form-control" placeholder="Recomendación">
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <input type="int" name="conclusion" class="form-control" placeholder="Conclusión">
                </div>
            </div>


            <div class="contenedor">
                <br>
                <div class="row justify-content-md-center">
                    <div class="col-4">
                        <input title="GUARDAR" type=image src="/images/guardar.png" width="50" height="50"
                            class="imagenpequeña">
                        <h5>Guardar</h5>

                    </div>
                    <div class="col-4">
                        <a title="CANCELAR" href="/evaluacion"><img src="/images/cancelar.png" class="pequeña"></a>
                        <h5>Cancelar</h5>
                    </div>
                </div>


        </form>
    


</div>
@endsection