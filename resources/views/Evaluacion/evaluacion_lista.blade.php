@section('title', "Centro de Salud")
@section('name', "Home")

@extends('layouts.app1')
@yield('sidebar')
@section('content')

<div class="container">
    <br><br>
    <h1>EVALUACIONES DE PACIENTES</h1>


    <a title="Nuevo Tratamiento" href="{{ route('evaluacion.create')}}"><img src="/images/evaluacion.png" class="imagenpequeña1"></a>
    <!-- <h5>Crear Evaluación</h5> -->
    <h5>
        <a title="Nuevo Tratamiento" href="{{ route('evaluacion.create')}}">Crear Tratamiento</a>
    </h5>

    <form>
        <div class="card-body row no-gutters align-items-center">
            <!--end of col-->
            <div class="col">
                <input class="form-control form-control-lg form-control-borderless" type="search"
                    placeholder="Buscar" name="search" id="search">
            </div>
            <!--end of col-->
            <div class="col-auto">
                <button class="btn btn-lg btn-success" type="submit">Buscar</button>
            </div>
            <!--end of col-->
        </div>
    </form>

</div><br>
<div class="table-responsive1">
    <table class="table small">
        <thead>
            <tr class="table-secondary">
            <th>Id</th>
            <th>Paciente</th>
            <th>Fecha</th>
            <th>Observación</th>
            <th>Recomendación</th>
            <th>Conclusión</th>
           
            <!-- <th>TERAPEUTA</th> -->
            <th>Opciones</th>
            </tr>
        </thead>
        @forelse($evaluacion  as $evaluacio)
        <tr>
            <td>{{ $evaluacio->id }}
            <td>{{ $evaluacio->paciente->nombre }}</td>
            <td>{{ $evaluacio->fecha }}</td>
            <td>{{ $evaluacio->observacion }}</td>
            <td>{{ $evaluacio->recomendacion }}</td>
            <td>{{ $evaluacio->conclusion }}</td>
            <td>
                <div class="container">
                    <div class="row justify-content-md-center">
                        <div class="col-">
                        <a title="Editar" href="{{route('evaluacion.edit', $evaluacio->id)}}"><img src="/images/editar.png"
                        class="imagenpequeña"></a>
                        </div>
                        <div class="col-">
                        <a title="PDF" href="{{ url('evaluacion/pdfexport/' . $evaluacio->id)}}" target="_blank"><img
                                src="/images/pdf.png" class="imagenpequeña"></a>
                        </div>
                        <div class="col-">
                        <form action="{{route('evaluacion.destroy', $evaluacio->id)}}" method="post">
                                @method('delete')
                                @csrf
                                <input title="Eliminar" type=image src="/images/eliminar.png" width="50" height="50"
                                    class="imagenpequeña"> </form>
                        </div>
                    </div>
                </div>
            </td>


        </tr>

        @empty


        <h5>No existe evaluaciones registradas</h5>

        @endforelse


    </table>
    <div class="paginate">
    {{$evaluacion->links()}}
    </div>

</div>



</div>
@endsection