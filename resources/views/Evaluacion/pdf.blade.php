

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>

<style>
h1 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
}

h2 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
}

h3 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
}

h4 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
}

p {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;

}

table {
    width: 100%;
    color: black;
    text-align: center;
    border-collapse: collapse;
    border: solid 1px #000000;
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;

}

table th {
    font-size: 13px;
    font-weight: normal;
    padding: 0px;
    color: black;
    border-top: 0px solid black;
    border-bottom: 1px solid black;

    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;

}
label{
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
line-height: 20pt;        /* para la separacion entre lineas */ 
text-indent: 30pt;        /* para sangrias */ 
}

/* Configuracion para la paginacion */
#header,
#footer {
  position: fixed;
  left: 0;
	right: 0;
	color: #aaa;
	font-size: 0.9em;
}
#header {
  top: 0;
	border-bottom: 0.1pt solid #aaa;
}
#footer {
  bottom: 0;
  border-top: 0.1pt solid #aaa;
}
.page-number:before {
  content: "Page " counter(page);
}

.left{
    float: left;
    
}

.center{
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
    font-size: 200%;
    }

img.pequeña {
    width: 30px;
    height: 30px;
    margin: auto;
    display: block;
}

#inferior{
position:absolute; /*El div será ubicado con relación a la pantalla*/
left:0px; /*A la derecha deje un espacio de 0px*/
right:0px; /*A la izquierda deje un espacio de 0px*/
bottom:100px; /*Abajo deje un espacio de 0px*/
height:0px; /*alto del div*/
z-index:0;
 }

</style>


<body>
<p>
<div class="left" id="center" class="pequeña">
<img src="{{ public_path('images/sigsig_logo.png') }}" class="pequeña">
</div>
<div class="center" id="center">
<!-- CENTRO DIURNO DE CUIDADO Y DESARROLLO INTEGRAL PARA PERSONAS CON DISCAPACIDAD -->
CENTRO DIURNO DE CUIDADO Y DESARROLLO INTEGRAL
</div>


<h2>EVALUACIÓN DE PACIENTE</h2><br>
<label>Paciente: </label>{{$pacientes->nombre}}<br>
<label>Fecha de Evaluación: </label>{{$evaluacion->fecha}}<br><br>


<!-- <label>Fecha de Nacimiento: </label>{{$pacientes->f_nacimiento}}<br> -->



<h4>ESTÍMULO</h4>
<table id="tabla">
            <thead>
                <tr>
                    <th>Estímulo</th>
                    <th>Fonema</th>
                    <th>Inicial</th>
                    <th>Media</th>
                    <th>Final</th>
                    <th>Transición</th>
                    <th>Respuesta</th>
                </tr>
            </thead>

            <tbody>
                @foreach($evaluacion->palabras as $palabra)
                
                <tr>
                    <td>{{$palabra->estimulo}}</td>
                    <td>{{$palabra->fonema}}</td>
                    <td>{{$palabra->inicial}}</td>
                    <td>{{$palabra->media}}</td>
                    <td>{{$palabra->final}}</td>
                    <td>{{$palabra->transicion}}</td>
                    <td>{{$palabra->respuesta}}</td>
                    
                </tr>
                @endforeach
                

            </tbody>

        </table>

        <h4>DIPTONGOS</h4>
        <table id="tabla1">
            <thead>
                <tr>
                    <th>Diptongos</th>
                    <th>Estímulo</th>
                    <th>Respuesta</th>
                    <th>Transcripción</th>

                </tr>
            </thead>

            <tbody>
                @foreach($evaluacion->diptongos as $diptongo)
                <tr>
                    <td>{{$diptongo->diptongo}}</td>
                    <td>{{$diptongo->estimulod}}</td>
                    <td>{{$diptongo->respuestad}}</td>
                    <td>{{$diptongo->transiciond}}</td>
                </tr>
                @endforeach

            </tbody>

            </table><br>
            
            <h4>DIFONOS</h4>
            <table id="tabla2">
            <thead>
                <tr>
                    <th>Difonos</th>
                    <th>Estímulo</th>
                    <th>Respuesta</th>
                    <th>Transcripción</th>
                </tr>
            </thead>

            <tbody>
                @foreach($evaluacion->difonos as $difono)
                <tr>
                    <td>{{$difono->difonos}}</td>
                    <td>{{$difono->estimulodf}}</td>
                    <td>{{$difono->respuestadf}}</td>
                    <td>{{$difono->transiciondf}}</td>
                </tr>
                @endforeach

            </tbody>
            </table>


                    <!-- TABLA 4 Diptongos -->

                    <h4>SILABAS INVERSAS</h4>
        <table id="tabla3">
            <thead>
                <tr>
                    <th>Inversa</th>
                    <th>Estímulo</th>
                    <th>Respuesta</th>
                    <th>Transcripción</th>

                </tr>
            </thead>

            <tbody>
                @foreach($evaluacion->silabasinversas as $silabasinversa)
                <tr>
                    <td>{{$silabasinversa->silabas}}</td>
                    <td>{{$silabasinversa->estimulods}}</td>
                    <td>{{$silabasinversa->respuestads}}</td>
                    <td>{{$silabasinversa->transicionds}}</td>
                </tr>
                @endforeach

            </tbody>

            </table>

            <br><br><br><br>

       

        <label>Observación:</label>
        {{$evaluacion->observacion}}<br>
        <label>Recomendación:</label>
        {{$evaluacion->recomendacion}}<br>
        <label>Conclusión:</label>
        {{$evaluacion->conclusion}}<br><br><br>


       
        

        <div id="inferior">
        <h4>___________________________</h4>
        <h4>{{ Auth::user()->name }}</h4>
        </div>

</p>

{{-- Here's the magic. This MUST be inside body tag. Page count / total, centered at bottom of page --}}
<script type="text/php">
    if (isset($pdf)) {
        $text = "page {PAGE_NUM} / {PAGE_COUNT}";
        $size = 10;
        $font = $fontMetrics->getFont("Verdana");
        $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
        $x = ($pdf->get_width() - $width) / 2;
        $y = $pdf->get_height() - 35;
        $pdf->page_text($x, $y, $text, $font, $size);
    }
</script>
</body>