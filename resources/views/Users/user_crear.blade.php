@section('title', "Centro de Salud")
@section('name', "Home")

@extends('layouts.app1')
@yield('sidebar')
@section('content')

<div class="container">
<form method="POST" action="{{ route('usuarios.store') }}" class="register-form">
        @method('post')
        @csrf
        <br>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <div>
                    <ul>
                        @foreach ($errors->all() as $message)
                        <h6><error>Error!</strong> {{ $message }}</h6>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
            <!-- cada uno de los ítems del grid -->
            <div class="login-page">
                <div class="form">
                    <h2>Creación de Usuario</h2>
                    <form class="register-form">
                        <input type="text" name="name" placeholder="Nombre" />
                        <input type="password" name="password" placeholder="Contraseña" />
                        <input type="text" name="email" placeholder="E-mail" />
                        <button type="submit">Guardar</button>
                </div>
            </div>
        </div>

    </form>



</div>
@endsection