@section('title', "Centro de Salud")
@section('name', "Home")

@extends('layouts.app1')
@yield('sidebar')
@section('content')

<div class="container">
    <br><br>
    <h3>LISTADO DE USUARIOS</h3>


    <a title="Nuevo Usuario" href="usuarios/create"><img src="/images/nuevo1.png" class="pequeña"></a>

    <h5>
        <a title="Nuevo Usuario" href="usuarios/create">Crear Usuario</a>
    </h5>

    <form>
        <div class="card-body row no-gutters align-items-center">
            <!--e   nd of col-->
            <div class="col">
                <input class="form-control form-control-lg form-control-borderless" type="search" placeholder="Buscar"
                    name="search" id="search">
            </div>
            <!--end of col-->
            <div class="col-auto">
                <button class="btn btn-lg btn-success" type="submit">Buscar</button>
            </div>
            <!--end of col-->
        </div>
    </form>

</div><br>
<div class="table-responsive1">
    <table class="table">
        <thead>
            <tr class="table-secondary">
                <th>Id</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Rol</th>
                <th>Opciones</th>
            </tr>
        </thead>
        @forelse($Users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->rol }}</td>
            <td>

                <div class="container">
                    <div class="row justify-content-md-center">
                        <div class="col-">
                            <a title="Editar" href="{{route('usuarios.edit', $user->id)}}"><img src="/images/editar.png"
                                    class="imagenpequeña"></a>
                        </div>
                        <div class="col-">
                            <form action="{{route('usuarios.destroy', $user->id)}}" method="post">
                                @method('delete')
                                @csrf
                                <input title="Eliminar" type=image src="/images/eliminar.png" class="imagenpequeña">
                            </form>
                        </div>
                    </div>
                </div>
            </td>


        </tr>

        @empty


        <h4>No hay Usuarios Registrados</h4>

        @endforelse


    </table>
    <div class="paginate">

    </div>

</div>



</div>
@endsection