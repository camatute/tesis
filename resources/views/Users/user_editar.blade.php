@section('title', "Centro de Salud")
@section('name', "Home")

@extends('layouts.app1')
@yield('sidebar')
@section('content')

<div class="container">
<form method="POST" action="{{ route('usuarios.update', $usuario->id) }}" class="register-form">
        @method('put')
        @csrf
        <br>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <div>
                    <ul>
                        @foreach ($errors->all() as $message)
                        <h6><error>Error!</strong> {{ $message }}</h6>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
            
            <div class="login-page">
                <div class="form">
                    <h2>EDICION DE USUARIO</h2>
                    <img src="/images/credenciales.png" class="pequeña" }}>
                    <form class="register-form">
                        <h4>NOMBRE</h4><input type="text" value="{{$usuario->name}}" name="name" placeholder="Nombre" />
                        <h4>CORREO ELECTRONICO</h><input type="text"  value="{{$usuario->email}}" name="email" placeholder="email" />
                        <button type="submit">Guardar</button>
                </div>
            </div>
        </div>

    </form>



</div>
@endsection