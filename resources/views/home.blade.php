<style>
img.pequeña {
    width: 30px;
    height: 29px;
    margin: auto;
    display: block;
}

.imagenpequeña {
    width: 30px;
    height: 30px;
    margin: auto;
    display: block;
}

.imagenpequeña1 {
    width: 40px;
    height: 40px;
    margin: auto;
    display: block;
}


img.mediana {
    width: 100px;
    height: 100px;
    margin: auto;
    display: block;
    /* filter: drop-shadow(5px 5px 10px #444); */
}

img.grande {
    width: 100%;
    height: 127px;
    margin: auto;
    display: block;
}

h4 {
    padding: 5px;
    text-align: center;
}



/* SIDE BAR */
</style>



@section('title', "Centro de Salud")
@section('name', "Home")

@extends('layouts.app1')
@yield('sidebar')
@section('content')
<br><br><br>
<div class="container">
    @if(Auth::user()->rol == 1)
    
<br><br><br>
    <div class="row">
        <div class="col-sm">
            <div class="column third">
                <a title="CREDENCIALES" href="{{ route('perfil.edit') }}"><img src="/images/credencial.png"
                        class="mediana"></a>

                <a href="{{ route('perfil.edit') }}">
                    <h4>PERFIL</h4>
                </a>
            </div>
        </div>

        <div class="col-sm">
            <div class="column third">
                <a title="CREDENCIALES" href="{{ route('usuarios.index') }}"><img src="/images/pacientes.png"
                        class="mediana"></a>

                <a href="{{ route('perfil.edit') }}">
                    <h4>Usuario</h4>
                </a>
            </div>
        </div>
    </div>


    @elseif(Auth::user()->rol == 2)
    <br>
    <div class="row">
        <div class="col-sm">
            <div class="column third">
                <a title="CREDENCIALES" href="{{ route('perfil.edit') }}"><img src="/images/credencial.png"
                        class="mediana"></a>

                <a href="{{ route('perfil.edit') }}">
                    <h4>PERFIL</h4>
                </a>
            </div>
        </div>
        <div class="col-sm">
            <a title="Pacientes" href="/paciente"><img src="/images/paciente.png" class="mediana"></a>
            <a href="/paciente">
                <h4>PACIENTE</h4>
            </a>
        </div>
        <div class="col-sm">
            <a title="TRATAMIENTO" href="/tratamiento"><img src="/images/tratamiento.svg" class="mediana"></a>
            <a href="/tratamiento">
                <h4>TRATAMIENTO</h4>
            </a>
        </div>
    </div>
</div>
<br><br>
<div class="container">
    <div class="row">
        <div class="col-sm">
            <a title="Pacientes" href="/tratamiento_lista"><img src="/images/material.png" class="mediana"></a>
            <a href="/tratamiento_lista">
                <h4>MATERIAL TRATAMIENTO</h4>
            </a>
        </div>
        <div class="col-sm">
            <a title="FAN PAGE" href="https://www.sigsig.gob.ec/centro-de-cuidado-y-desarrollo-integral-para-personas-con-discapacidad/" target="_blank"><img src="/images/sigsig.png" class="mediana"></a>
            <a href="https://www.sigsig.gob.ec/centro-de-cuidado-y-desarrollo-integral-para-personas-con-discapacidad/" target="_blank">
                <h4>www.sigsig.gob.ec</h4>
            </a>
        </div>
        <div class="col-sm">
            <a title="EVALUACION" href="/evaluacion"><img src="/images/registro.png" class="mediana"></a>
            <a href="/evaluacion">
                <h4>EVALUACIÓN</h4>
            </a>
        </div>
    </div>
</div>
@endif


@endsection

@yield('footer')