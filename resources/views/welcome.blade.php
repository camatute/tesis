
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Centro de Salud</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
    html,
    body {
        overflow: hidden;
        height: 100%;

    }
    body {
    margin: 0;
    padding: 0;
    opacity: 0.9;
    background-image: url(images/welcome.jpg);

    /* Para dejar la imagen de fondo centrada, vertical y

horizontalmente */

    background-position: center center;

    /* Para que la imagen de fondo no se repita */

    background-repeat: no-repeat;

    /* La imagen se fija en la ventana de visualización para que la altura de la imagen no supere a la del contenido */

    background-attachment: fixed;

    /* La imagen de fondo se reescala automáticamente con el cambio del ancho de ventana del navegador */

    background-size: cover;

    /* Se muestra un color de fondo mientras se está cargando la imagen

de fondo o si hay problemas para cargarla */

    background-color: #66999;
}

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
        color: white;
        font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;

    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }


    }

    .title {
        font-size: 40px;
        text-align: center;
        font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;

    }

    .links>a {
        color:  white;
        
        font-size: 13px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
        text-align: center;
        
        font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;


    }

    .linkss>a {
        color:  white;
        
        font-size: 13px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
        text-align: center;
        padding-left: 50%;
        
        font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;


    }

    .m-b-md {
        margin-bottom: 20px;
        
    }

    h1 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
    font-size: 4em;
}

h2 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
    font-size: 1.5em;
}

h3 {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;
}
    </style>
</head>

<body>
<div class="container portada">
    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/home') }}">Principal</a>
            @else
            <a href="{{ route('login') }}">Logueate</a>
            <!-- @if (Route::has('register'))
            <a href="{{ route('register') }}">Register</a>
            @endif -->
            @endauth
        </div>
        @endif

        <div class="content">
            <div class="title m-b-md">
                <h1>ASISTENTE DIGITAL PARA FONÓAUDIOLOGOS</h1>
               <h2>CENTRO DE CUIDADO Y DESARROLLO INTEGRAL PARA PERSONAS CON DISCAPACIDAD</h2> 
            </div>

            <div class="linkss">
               <h3>
               <a href="https://www.sigsig.gob.ec/centro-de-cuidado-y-desarrollo-integral-para-personas-con-discapacidad/" target="_blank">www.sigsig.gob.ec</a>

               </h3> 
            </div>
        </div>
        </div>
        </div>
    
</body>

</html>