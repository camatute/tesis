<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Administrador del sistema
            DB::table('users')->insert([
                'name' => 'SuperAdmin',
                'email' => 'carlos.albertomatute02@gmail.com',
                'password' => bcrypt('password'),
                'rol' => '1',
                'created_at' => today(),
                'updated_at' => today(),
                ]);

            DB::table('users')->insert([
                'name' => 'Admin',
                'email' => 'admsigsigcentrosalud@gmail.com',
                'password' => bcrypt('admin-sigsig2020'),
                'rol' => '1',
                'created_at' => today(),
                'updated_at' => today(),
                ]);
                
            DB::table('users')->insert([
                'name' => 'Terapeuta',
                'email' => 'guala155abc@gmail.com',
                'password' => bcrypt('password'),
                'rol' => '2',
                'created_at' => today(),
                'updated_at' => today(),
                ]);      
        }
    }