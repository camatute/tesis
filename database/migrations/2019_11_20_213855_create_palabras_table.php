<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePalabrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('palabras', function (Blueprint $table) {
            $table->Increments('id');
            $table->unsignedBigInteger('id_r');
            $table->foreign('id_r')->references('id')->on('registros')-> onDelete('cascade');
            $table->String('estimulo')->nullable();
            $table->String('fonema')->nullable();
            $table->String('inicial')->nullable();
            $table->String('media')->nullable();
            $table->String('final')->nullable();
            $table->String('transicion')->nullable();
            $table->String('respuesta')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('palabras');
    }
}
