<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTerapiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terapias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_u');
            $table->foreign('id_u')->references('id')->on('users')-> onDelete('cascade');
            $table->String('imagen')->nullable();
            $table->String('palabra')->nullable();
            $table->String('audio')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terapias');
    }
}
