<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiptongosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diptongos', function (Blueprint $table) {
            $table->Increments('id');

            $table->unsignedBigInteger('id_r');
            $table->foreign('id_r')->references('id')->on('registros')-> onDelete('cascade');

            $table->String('diptongo')->nullable();
            $table->String('estimulod')->nullable();
            $table->String('respuestad')->nullable();
            $table->String('transiciond')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diptongos');
    }
}
