<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_u');
            $table->foreign('id_u')->references('id')->on('users')-> onDelete('cascade');
            $table->String('nombre');
            $table->Date('f_nacimiento');
            $table->Integer('edad');
            $table->String('direccion')->nullable();
            $table->Integer('telefono')->nullable();
            $table->String('referencia')->nullable();
            $table->String('motivo')->nullable();
            $table->String('fonacion')->nullable();
            $table->String('lenguaje')->nullable();
            $table->String('habla')->nullable();
            $table->String('audicion')->nullable();
            $table->String('otro')->nullable();
            $table->String('n_madre')->nullable();;/*->coment('Nombre Madre'); */
            $table->Integer('e_madre')->nullable();;/*->coment('Edad Madre');*/
            $table->String('o_madre')->nullable();;/*->coment('Ocupacion Madre');*/
            $table->String('n_padre')->nullable();;/*->coment('Nombre Padre');*/
            $table->Integer('e_padre')->nullable();;/*->coment('Edad Padre');*/
            $table->String('o_padre')->nullable();;/*->coment('Ocupacion Padre');*/


            $table->String('observacion')->nullable();
            $table->String('recomendacion')->nullable();
            $table->String('conclusion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}