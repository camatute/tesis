<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSilabasinversasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('silabasinversas', function (Blueprint $table) {
            $table->Increments('id');

            $table->unsignedBigInteger('id_r');
            $table->foreign('id_r')->references('id')->on('registros')-> onDelete('cascade');

            $table->String('silabas')->nullable();
            $table->String('estimulods')->nullable();
            $table->String('respuestads')->nullable();
            $table->String('transicionds')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('silabasinversas');
    }
}
