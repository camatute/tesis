<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMiembrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('miembros', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('id_p');
            $table->foreign('id_p')->references('id')->on('pacientes')-> onDelete('cascade');
            $table->String('m_casa')->nullable();
            $table->String('m_edad')->nullable();
            $table->String('m_sexo')->nullable();
            $table->String('m_relniño')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('miembros');
    }
}
