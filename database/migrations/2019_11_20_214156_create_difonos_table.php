<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDifonosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('difonos', function (Blueprint $table) {
            $table->Increments('id');

            $table->unsignedBigInteger('id_r');
            $table->foreign('id_r')->references('id')->on('registros')-> onDelete('cascade');

            $table->String('difonos')->nullable();
            $table->String('estimulodf')->nullable();
            $table->String('respuestadf')->nullable();
            $table->String('transiciondf')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('difonos');
    }
}
